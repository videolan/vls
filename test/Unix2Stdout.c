/*****************************************************************************/
/*                                                                           */
/* Unix Socket Client                                                        */
/* Program to test the UnixSocket stream output                              */
/*                                                                           */
/* Tristan Leteurtre <tristan.leteurtre@anevia.com>                          */
/* Oct 2003                                                                  */
/*                                                                           */
/*****************************************************************************/

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>  /* exit malloc */
#include <unistd.h> /* close */


int main(int argc, char ** argv)
{ 
  int sock;
  struct sockaddr_un server, client;

  if (argc < 2)
  { 
     printf("usage: %s <pathname>\n", argv[0]);
     printf("       outputs the stream of <pathname> to stdout\n");
     exit(1);
  }

  sock = socket(AF_UNIX, SOCK_DGRAM, 0);
  if (sock < 0)
  { 
    perror("opening stream socket");
    exit(1);
  }
  
  server.sun_family = AF_UNIX;
  strncpy(server.sun_path, argv[1], 108);
  
 
  unlink(server.sun_path);

  printf("unlink %s\n", server.sun_path);
  
  if (bind(sock, (struct sockaddr *) &server,
                    sizeof(struct sockaddr_un)) < 0)
  {
    close(sock);
    perror("Error binding socket");
    exit(1);
  }
  else
    printf("Socket bound sockid %d\n", sock);

  char * s;
  s = (char*)malloc (1316 * sizeof(char));
  int irc;
  int clientlen = sizeof(client);
  
  while(12)
  {
    if ((irc = recvfrom(sock, s, 1316, 0,  (struct sockaddr *)&client,
                                       &clientlen)) < 0)
      perror("Erro receving on socket");
    write(STDOUT_FILENO, s, 1316);
    /* Maybe sendfile is better, but I don't care at all */
  }

  close(sock);
  unlink(server.sun_path);

  return 0;
} 

