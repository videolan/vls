#!/bin/sh

# test script for the mpeg detection test suite
# $Id: test.sh,v 1.1 2003/08/24 20:19:26 lool Exp $

# TODO check the output of the commands

VLS="../../bin/vls"
VLS_OPT="-vvv -d 127.0.0.1"

$VLS $VLS_OPT file:test1.mpeg2-ts
$VLS $VLS_OPT file:test2.mpeg1.ps
$VLS $VLS_OPT file:test3.mpeg2-ps
$VLS $VLS_OPT file:test4.mpeg2-ts
$VLS $VLS_OPT file:test5.pes
$VLS $VLS_OPT file:test6.raw

