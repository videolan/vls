/*******************************************************************************
* telnetinterface.cpp:
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*          Tristan Leteurtre <tristan.leteurtre@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Informations on the Telnet protocol can be found in RFC 854 to 861 and also
* RFC 1184 for linemodes
*
*******************************************************************************/



//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include <ctype.h>

#include "../../core/core.h"
#include "../../core/network.h"
#include "../../server/request.h"
#include "../../server/repository.h"
#include "../../server/admin.h"
#include "../../server/interface.h"
#include "telnetinterface.h"

#include "../../core/network.cpp"

#include "../../server/repository.cpp"

//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_TelnetInterfaceModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_telnetinterface(handle hLog)
{
  return new C_TelnetInterfaceModule(hLog);
}
#endif



//------------------------------------------------------------------------------
// Telnet interface
//------------------------------------------------------------------------------
#define WELCOME         "Videolan Server Administration System"
#define LOGIN           "Login: "
#define PASSWD          "Password: "
#define PROMPT          "vls:"
#define PROMPTEND       "# "

//------------------------------------------------------------------------------
// Telnet protocol constants
//------------------------------------------------------------------------------
#define IAC     255             // Interpret as command
#define DONT    254             // You are not to use option
#define DO      253             // Please use option
#define WONT    252             // I won't use option
#define WILL    251             // I will use option
#define EL      248             // Erase the current line

#define TELOPT_ECHO     1       // Echo
#define TELOPT_SGA      3       // Suppress go ahead
#define TELOPT_OUTMRK   27      // Output marking
#define TELOPT_LINEMODE 34      // Linemode option

#define KEY_UP          65      //
#define KEY_DOWN        66      //
#define KEY_LEFT        67      //
#define KEY_RIGHT       68      //


//------------------------------------------------------------------------------
// Telnet module definitions
//------------------------------------------------------------------------------
#define CONFIG_MODE             1
#define CONTROL_MODE            2
#define INTERACTIVE_MODE        3

#define INIT_PHASE              1
#define LOGIN_PHASE             2
#define PASSWD_PHASE            3
#define COMMAND_PHASE           4

#define HISTORY_SIZE            20



/*******************************************************************************
* E_TelnetInterface exception
********************************************************************************
*
*******************************************************************************/
E_TelnetInterface::E_TelnetInterface(const C_String& strMsg) : E_Exception(GEN_ERR, strMsg)
{
}

E_TelnetInterface::E_TelnetInterface(const C_String& strMsg, const E_Exception& e) :
             E_Exception(GEN_ERR, strMsg, e)
{
}


/*******************************************************************************
* C_TelnetInterfaceOptions class
********************************************************************************
* Manage the configuration of the 2 NVT through telnet negociation
* Very basic at that time, is to be completly rewritten
*******************************************************************************/

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Buffer<byte> C_TelnetInterfaceOptions::Request(byte bAction, byte bOption)
{
  C_Buffer<byte> cAnswer(3);
  cAnswer.SetSize(3);
  cAnswer[0] = IAC;
  cAnswer[1] = bAction;
  cAnswer[2] = bOption;

  //m_cPendingRequests.PushEnd(new C_Buffer<byte>(cAnswer));

  return cAnswer;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Buffer<byte> C_TelnetInterfaceOptions::Answer(const C_Buffer<byte>& cRequest)
{
  ASSERT(cRequest.GetSize() == 3);
  (void)cRequest;

  C_Buffer<byte> cAnswer(3);
  cAnswer.SetSize(0);

  //  cAnswer.SetSize(3);
  //  cAnswer[0] = IAC;
  //  cAnswer[2] = cRequest[3];

  return cAnswer;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int C_TelnetInterfaceOptions::GetOptionStatus()
{
  return YES;
}





/*******************************************************************************
* C_TelnetInterfaceCmdLine class
********************************************************************************
* Manage a telnet command line
*******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_TelnetInterfaceCmdLine::C_TelnetInterfaceCmdLine() : m_cCurrent(m_cHistory.CreateIterator())
{
  m_iPosInLine = 0;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_String C_TelnetInterfaceCmdLine::Append(byte bByte)
{
  C_String cResult;

  switch(bByte)
  {
    case 0x7F:
    {
      if(m_strLine.Length() > 0)
      {
        m_strLine = m_strLine.SubString(0, m_strLine.Length()-1);
        cResult += "\b\e[P";
      }
      break;
    }
    default:
    {
      if(bByte != '\n' && bByte != '\0')
      {
        m_strLine += (char)bByte;
        cResult += (char)bByte;
      }
    }
  }

  return cResult;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Buffer<byte> C_TelnetInterfaceCmdLine::Edit(const C_Buffer<byte>& cCmd)
{
  ASSERT(cCmd.GetSize() == 3);
  ASSERT(cCmd[0] = TELOPT_OUTMRK);

  C_String strResult;

  switch(cCmd[2])
  {
    case KEY_UP:
    {
      if(m_cCurrent.HasNext())
      {
         strResult = *m_cCurrent.GetNext();
      }
      break;
    }
    case KEY_DOWN:
    {
      if(m_cCurrent.HasPrevious())
      {
        strResult = *m_cCurrent.GetPrevious();
      }
      break;
    }
  }

  if(strResult != "")
  {
    StartNewLine();
    m_strLine = strResult;
  }

  return /*EL + strResult*/C_Buffer<byte>(0);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_String C_TelnetInterfaceCmdLine::GetCmdLine(C_String strPath)
{
  // Strip line
  C_String strResult = m_strLine.Strip(" \t");
  if(strPath!="" && strResult.SubString(0,2) != "cd")
  {
    strResult = strPath + "_" + strResult;
  }
  return strResult;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_TelnetInterfaceCmdLine::StartNewLine()
{
  // Store current line if not empty
  C_String strLine = GetCmdLine("");
  if(strLine.Length() > 0)
  {
    m_cHistory.PushStart(new C_String(strLine));
    // To do: removal of old commands
  }

  // Reset command history
  m_cCurrent.Reset();

  // Empty current line
  m_strLine = "";
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_TelnetInterfaceCmdLine::DropLine()
{
  m_strLine = "";
}




/*******************************************************************************
* C_TelnetInterfaceSession class
********************************************************************************
*
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_TelnetInterfaceSession::C_TelnetInterfaceSession(C_Socket* pConnection, void* pAdmin) :
                      C_InterfaceSession(pConnection,pAdmin),
                      m_strPeerName("[Unknown]"),
                      m_cBuffer(3)
{
  m_iMode = INTERACTIVE_MODE;
  //m_iPhase = INIT_PHASE;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_TelnetInterfaceSession::OnInit()
{
  // Actual initialisation is done internally in small pieces cause it could
  // block the entire server if done in one single step. For now just send
  // the welcome message
  m_cStream << "\r\n" << WELCOME << "\r\n\r\n";

  // Transmission parameters: to be able to deal with all clients, editing is
  // done on the server
  m_cStream << m_cOptions.Request(WILL, TELOPT_ECHO);
  m_cStream << m_cOptions.Request(WILL, TELOPT_SGA);
  m_cStream << m_cOptions.Request(DONT, TELOPT_LINEMODE);

  m_cStream << LOGIN;
  m_iPhase = LOGIN_PHASE;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_TelnetInterfaceSession::OnClose()
{
  try
  {
    // Send a goodby message if possible
    if(m_cStream.GetState() == STREAM_READY)
      m_cStream << "\r\nClosing connection" << "\r\n";

    // Close the connection
    m_pConnection->Close();
  }
  catch(E_Exception e)
  {
    throw E_TelnetInterface("Unable to close connection with "+m_strPeerName, e);
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_TelnetInterfaceSession::ProcessData()
{
  try
  {
    // First get the data from the network
    C_Buffer<byte> cData(256);
    m_cStream >> cData;
    // There must be some data because the select told us else the client
    // closed his session.
    if(cData.GetSize() <= 0)
      throw E_TelnetInterface("Connection reset by peer");

    unsigned int iPos = 0;
    unsigned int iSize = cData.GetSize();

    while(iPos < iSize)
    {
      switch(cData[iPos])
      {
        case IAC:
        {
          // Telnet negociation or special char
//          printf("negociation: %x %x\n", cData[iPos+1], cData[iPos+2]);
          m_cBuffer[0] = IAC;
          m_cBuffer.SetSize(1);
          m_iMode = CONFIG_MODE;
          break;
        }

        case TELOPT_OUTMRK:
        {
          // Telnet special chars
//          printf("output marking: %x %x\n", cData[iPos+1], cData[iPos+2]);
          m_cBuffer[0] = TELOPT_OUTMRK;
          m_cBuffer.SetSize(1);
          m_iMode = CONTROL_MODE;
          break;
        }

        case '\r':
        {
          // End of line sent: handle the corresponding data
          HandleLine();

          // Send prompt for next line
          m_cStream << "\r\n" << m_strPrompt;
          break;
        }

        default:
        {
          HandleByte(cData[iPos]);
          break;
        }
      }

      // Go to next char in input
      iPos++;
    }
  }
  catch(E_Stream<C_Socket> e)
  {
    throw E_TelnetInterface("Connection error", e);
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_TelnetInterfaceSession::HandleLine()
{
  switch(m_iPhase)
  {
    case LOGIN_PHASE:
    {
      // Execute first login phase
      ExecLogin();
      // Drop line since we don't have to remember login
      m_cCmdLine.DropLine();
      break;
    }
    case PASSWD_PHASE:
    {
      // Check login
      ExecLogin();
      // Drop line since we don't have to remember passwd
      m_cCmdLine.DropLine();
      break;
    }
    default:
    {
      ASSERT(m_iPhase == COMMAND_PHASE);
      // Execute command
      ExecCommand();
      // Start a new line
      m_cCmdLine.StartNewLine();
      break;
    }
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_TelnetInterfaceSession::HandleByte(byte bByte)
{
  //printf("char: '%c' (%x)\n", bByte, bByte);

  switch(m_iMode)
  {
    case CONFIG_MODE:
    {
      ASSERT(m_cBuffer[0] == IAC);
      unsigned int iSize = m_cBuffer.GetSize();
      m_cBuffer[iSize] = bByte;
      iSize++;
      m_cBuffer.SetSize(iSize);
      if(iSize == 3)
      {
        m_cStream << m_cOptions.Answer(m_cBuffer);
        m_cBuffer.SetSize(0);
        m_iMode = INTERACTIVE_MODE;
      }
      break;
    }
    case CONTROL_MODE:
    {
      ASSERT(m_cBuffer[0] == TELOPT_OUTMRK);
      unsigned int iSize = m_cBuffer.GetSize();
      m_cBuffer[iSize] = bByte;
      iSize++;
      m_cBuffer.SetSize(iSize);
      if(iSize == 3)
      {
        m_cStream << m_cCmdLine.Edit(m_cBuffer);
        m_cBuffer.SetSize(0);
        m_iMode = INTERACTIVE_MODE;
      }
      break;
    }
    default:
    {
      ASSERT(m_iMode == INTERACTIVE_MODE);

      // Drop all characters before init is completed
      int iStatus = m_cOptions.GetOptionStatus();
      if(iStatus == YES)
      {
        // Remote echo
        C_String cRemoteCmdLine = m_cCmdLine.Append(bByte);
        if(m_iPhase != PASSWD_PHASE)
          m_cStream << cRemoteCmdLine;
      }
      else if(iStatus == NO)
        throw E_TelnetInterface("Client not supported");

      //printf("char: %c\n", bByte);
      break;
    }
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_TelnetInterfaceSession::ExecLogin()
{
  ASSERT(m_iPhase == LOGIN_PHASE || m_iPhase == PASSWD_PHASE);

  if(m_iPhase == LOGIN_PHASE)
  {
    // This is the login which has just been entered
    m_strLogin = m_cCmdLine.GetCmdLine("");
    m_strPrompt = PASSWD;
    m_iPhase = PASSWD_PHASE;
  }
  else
  {
    // Login has been entered before, this is the passwd
    C_String strPasswd = m_cCmdLine.GetCmdLine("");

    // Authentication (to do)
    int iRc = Authenticate(m_strLogin, strPasswd);

    if(!iRc)
    {
      m_strPrompt = m_strLogin + "@" + PROMPT + m_strPath + PROMPTEND;
      m_iPhase = COMMAND_PHASE;
    }
    else
    {
      m_strPrompt = LOGIN;
      m_iPhase = LOGIN_PHASE;
    }

    m_cStream << "\r\n";
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_TelnetInterfaceSession::ExecCommand()
{
  ASSERT(m_iPhase == COMMAND_PHASE);

  C_String strCmd = m_cCmdLine.GetCmdLine(m_strPath);

  C_Request cRequest("");
  C_Answer cAdminAnswer = m_pAdmin->ParseCmdLine(this, strCmd, cRequest);
  switch(cAdminAnswer.GetStatus())
  {
  case ADMIN_WELLFORMED_COMMAND:
    if(cRequest.GetCmd() == "logout")
    {
      throw E_TelnetInterface("logout requested");
    }
    else if (cRequest.GetCmd() == "cd")
    {
        // TODO find something a bit more generic ;-)
        // nitrox
        if(m_strPath == "" && cRequest.GetArg("path") == "broadcast")
        {
            m_strPath = "broadcast";
        }
        else if( cRequest.GetArg("path") == "..")
        {
            m_strPath = "";
        }
        else if(  m_strPath == "" && cRequest.GetArg("path") == "channel")
        {
            m_strPath = "channel";
        }
        else if(  m_strPath == "" && cRequest.GetArg("path") == "input")
        {
            m_strPath = "input";
        }
        else if(  m_strPath == "" && cRequest.GetArg("path") == "program")
        {
            m_strPath = "program";
        }
        m_strPrompt = m_strLogin + "@" + PROMPT + m_strPath + PROMPTEND;
    }
    else
    {
      C_Answer cAnswer = m_pAdmin->HandleRequest(cRequest);
      SendAnswer(cAnswer);
    }
    break;
  case ADMIN_COMMAND_NOT_VALID:
  case ADMIN_UNKNOWN_COMMAND:
  case ADMIN_COMMAND_DENIED:
    SendAnswer(cAdminAnswer);
    break;
  case ADMIN_EMPTY_COMMAND:
    // Nothing to do
    break;
  default:
    ASSERT(false);
    break;
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// Send a C_Answer in human readable format
//------------------------------------------------------------------------------
void C_TelnetInterfaceSession::SendAnswer(const C_Answer& cAnswer,
                                 const C_String& strPrefix)
{
  if(cAnswer.GetProvider() != "")
  {
    m_cStream << "\r\n";
#ifdef DEBUG
    m_cStream << strPrefix + "Provider: " + cAnswer.GetProvider() + "\r\n";

    int iStatus = cAnswer.GetStatus();
    if(iStatus >= 0)
      m_cStream << strPrefix + "Status: " + iStatus + "\r\n";
    else
      m_cStream << strPrefix + "Error: " + iStatus + "\r\n";
#endif
    C_List<C_String> cMessageList = cAnswer.GetMessages();
    unsigned int iSize = cMessageList.Size();
    for(unsigned int i = 0; i < iSize; i++)
    {
#ifdef DEBUG
      m_cStream << strPrefix + "Info: " + cMessageList[i] + "\r\n";
#else
      m_cStream << strPrefix + cMessageList[i] + "\r\n";
#endif
    }

    C_List<C_Answer> cAnswerList = cAnswer.GetSubAnswers();
    iSize = cAnswerList.Size();
    for (unsigned int j = 0; j < iSize; j++)
    {
      SendAnswer(cAnswerList[j], strPrefix+"  ");
    }
  }
}





/*******************************************************************************
* C_TelnetInterface class
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_TelnetInterface::C_TelnetInterface(C_Module* pModule, C_InterfaceConfig& cConfig) :
              C_Interface(pModule, cConfig),
              C_ConnectionsHandler<C_TelnetInterfaceSession>(cConfig.hLog,cConfig.pAdmin)
{
}

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
int C_TelnetInterface::Init()
{
  C_Application* pApp = C_Application::GetApp();
  ASSERT(pApp);

  int iRc = NO_ERR;

  // Get config
  C_String strDomain = pApp->GetSetting(m_sInterfaceName+".domain", "inet4");

  int iDomain;
  C_String strDefaultHost;

  if(strDomain == "inet4")
  {
    iDomain = AF_INET;
    strDefaultHost = "0.0.0.0";
  }
#ifdef HAVE_IPV6
  else if(strDomain == "inet6")
  {
    iDomain = AF_INET6;
    strDefaultHost = "0::0";
  }
#endif
  else
  {
    Log(C_Interface::m_hLog, LOG_ERROR, "Unknown domain:\n" + strDomain);
    iRc = GEN_ERR;
  }

  if(!iRc)
  {
    C_String strAddr = pApp->GetSetting(m_sInterfaceName+".localaddress",
                                                               strDefaultHost);
    C_String strPort = pApp->GetSetting(m_sInterfaceName+".localport", "9999");

    // Init the socket
    try
    {
      C_ConnectionsHandler<C_TelnetInterfaceSession>::Init(iDomain, strAddr, strPort);
    }
    catch(E_Exception e)
    {
      Log(C_Interface::m_hLog, LOG_ERROR,
          m_sInterfaceName+" server initialisation failed:\n" + e.Dump());
      iRc = GEN_ERR;
    }

    if(!iRc)
    {
     Log(C_Interface::m_hLog, LOG_NOTE, m_sInterfaceName+" server initialised");
    }
  }
  return iRc;
}

void C_TelnetInterface::InitSession()
{
}

//------------------------------------------------------------------------------
// Execution
//------------------------------------------------------------------------------
void C_TelnetInterface::RunSession()
{
    C_ConnectionsHandler<C_TelnetInterfaceSession>::Run();
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_TelnetInterface::StopSession()
{
    C_ConnectionsHandler<C_TelnetInterfaceSession>::Stop();
}


//------------------------------------------------------------------------------
// Destruction
//------------------------------------------------------------------------------
void C_TelnetInterface::DestroySession()
{
    C_ConnectionsHandler<C_TelnetInterfaceSession>::Destroy();
}

