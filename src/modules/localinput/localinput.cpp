/*******************************************************************************
* localinput.cpp: Local streams
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: localinput.cpp,v 1.22 2003/10/27 10:58:10 sam Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../../core/core.h"

#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"
#include "../../mpeg/rtp.h"
#include "../../server/program.h"
#include "../../server/buffer.h"
#include "../../server/output.h"
#include "../../server/channel.h"
#include "../../server/broadcast.h"
#include "../../server/request.h"
#include "../../server/input.h"
#include "../../server/tsstreamer.h"

#include "../../mpeg/reader.h"
#include "../../mpeg/trickplay.h"
#include "../../mpeg/converter.h"

#include "localinput.h"

#define MAX_FILES_NUMBER 4096

//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_LocalInputModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_localinput(handle hLog)
{
  return new C_LocalInputModule(hLog);
}
#endif


/*******************************************************************************
* C_LocalInput class
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_LocalInput::C_LocalInput(C_Module* pModule,
                           const C_String& strName) : C_Input(pModule,
                                                              strName)
{
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_LocalInput::~C_LocalInput()
{
}


//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
void C_LocalInput::OnInit()
{
  C_Settings m_cSettings;
  C_Application* pApp = C_Application::GetApp();
  ASSERT(pApp);

  // Build the program list

  for(int ui = 1;
       ui <= pApp->GetSetting(GetName()+".programcount", "0").ToInt(); ui++)
  {
    C_String strName = pApp->GetSetting(GetName()+"."+C_String(ui)
                                           + ".name",  C_String("Pgrm") + ui);
    C_String* pStr = new C_String(strName);
    m_vProgramNames.Add(pStr);
    Log(m_hLog, LOG_NOTE, "Added program '" + *pStr+"'");
  }
}


//------------------------------------------------------------------------------
// Destruction
//------------------------------------------------------------------------------
void C_LocalInput::OnDestroy()
{
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_List<C_Program> C_LocalInput::OnGetAvailablePgrms()
{
  C_List<C_Program> cPgrmList;

  for(unsigned int ui = 0; ui < m_vProgramNames.Size(); ui++)
  {
    C_Program* pProgram = new C_Program(m_vProgramNames[ui]);
    ASSERT(pProgram);
    cPgrmList.PushEnd(pProgram);
  }

  return cPgrmList;
}


//------------------------------------------------------------------------------
// Start the reception of the given program
//------------------------------------------------------------------------------
void C_LocalInput::OnStartStreaming(C_Broadcast* pBroadcast)
{
  C_Application* pApp = C_Application::GetApp();
  ASSERT(pApp);
  ASSERT(pBroadcast);

  // We choose a TS packet buffer able to store up to 3s of stream at 8 Mbits/s
  C_NetList* pTsProvider = new C_NetList(3*3*797);

  // The netlist must be at least of the size of the Reader buffer +
  // the size of the output buffer + 2 to be sure that there are always free
  // packets in it
  const C_Channel* pChannel = pBroadcast->GetChannel();
  ASSERT(pTsProvider->Capacity() - pChannel->GetBuffCapacity() - 2 > 0);
  unsigned int uiSize = pTsProvider->Capacity() -pChannel->GetBuffCapacity()-2;

  C_SyncFifo* pBuffer;

  // Get the type of the program
  unsigned int uiId =
        m_vProgramNames.Find(pBroadcast->GetProgram()->GetName()) + 1;
  ASSERT(uiId > 0);
  C_String strId = GetName() + "." + uiId;
  C_String strType = pApp->GetSetting(strId + ".type", "mpeg2-ts");

  C_String strReaderType;
  C_String strConverterType;
  C_String strTrickPlayType;

  // Specific behaviour depends on the type
  if(strType == "mpeg1-ps")             // Mpeg 1 - Program Stream
  {
    // Update the size of the buffer and create it
    uiSize -= 2;
    pBuffer = new C_SyncFifo(uiSize);
    // Reader configuration
    strReaderType = "file";
    C_String strFile = m_strFilesPath +
                                   pApp->GetSetting(strId+".filename", "");
    pBroadcast->SetOption("filename", strFile);

    // Converter configuration
    strConverterType = "ps2ts";
    pBroadcast->SetOption("mpegversion", "1");
    pBroadcast->SetOption("preparse", "1");
  }
  else if(strType == "mpeg2-ps")        // Mpeg 2 - Program Stream
  {
    // Update the size of the buffer and create it
    uiSize -= 2;
    pBuffer = new C_SyncFifo(uiSize);
    // Reader configuration
    strReaderType = "file";
    C_String strFile = m_strFilesPath +
                       pApp->GetSetting(strId+".filename", "");
    pBroadcast->SetOption("filename", strFile);

    // Converter configuration
    strConverterType = "ps2ts";
    pBroadcast->SetOption("mpegversion", "2");
    pBroadcast->SetOption("preparse", "1");
  }
  else if(strType == "mpeg2-ts")        // Mpeg 2 - Transport Stream
  {
    // Update the size of the buffer and create it
    pBuffer = new C_SyncFifo(uiSize);
    // Reader configuration
    strReaderType = "file";
    C_String strFile = m_strFilesPath + pApp->GetSetting(strId+".filename", "");
    pBroadcast->SetOption("filename", strFile);

    // Converter configuration
    strConverterType = "ts2ts";
  }
  else if(strType == "dvd")             // DVD device (Mpeg2 PS)
  {
    // Update the size of the buffer and create it
    uiSize -= 2;
    pBuffer = new C_SyncFifo(uiSize);
    // Reader configuration
    strReaderType = "dvd";
    C_String strDevice = pApp->GetSetting(strId+".device", "");
    pBroadcast->SetOption("device", strDevice);
    C_String strDvdTitle = pApp->GetSetting(strId+".dvdtitle","");
    pBroadcast->SetOption("dvdtitle", strDvdTitle);
    C_String strDvdChapter = pApp->GetSetting(strId+".dvdchapter","");
    pBroadcast->SetOption("dvdchapter", strDvdChapter);

    // Converter configuration
    strConverterType = "ps2ts";
    pBroadcast->SetOption("mpegversion", "2");
    pBroadcast->SetOption("preparse", "0");
  }
  else
  {
    delete pTsProvider;
    throw E_Exception(GEN_ERR, "Unsupported or unknown type : " + strType);
  }

  C_MpegReader* pReader;

  // Create the reader
  C_MpegReaderModule* pReaderModule = (C_MpegReaderModule*)
                                C_Application::GetModuleManager()
                                        ->GetModule("mpegreader",
                                                    strReaderType);

  if(pReaderModule)
  {
    pReader = pReaderModule->NewMpegReader(pBroadcast);
    ASSERT(pReader);
  }
  else
  {
    throw E_Exception(GEN_ERR,
                      "Module mpegreader:" + strConverterType +
                      " not present");
  }

  // Create the converter
  C_MpegConverter* pConverter;
  C_MpegConverterModule* pConverterModule = (C_MpegConverterModule*)
                                C_Application::GetModuleManager()
                                        ->GetModule("mpegconverter",
                                                    strConverterType);

  if(pConverterModule)
  {
    C_MpegConverterConfig cConfig;
    cConfig.m_hLog = m_hLog;
    cConfig.m_pBroadcast = pBroadcast;
    cConfig.m_pReader = pReader;
    cConfig.m_pTsProvider = pTsProvider;
    cConfig.m_pEventHandler = m_pEventHandler;
    pConverter = pConverterModule->NewMpegConverter(cConfig);
    ASSERT(pConverter);
  }
  else
  {
    throw E_Exception(GEN_ERR,
                      "Module mpegconverter:" + strConverterType +
                      " not present");
  }
  pReader->SetConverter(pConverter);

  // Create the TrickPlay
  C_TrickPlay* pTrickPlay;
  strTrickPlayType = pApp->GetSetting(GetName()+".trickPlay",
                      "normal").ToLower();

  C_TrickPlayModule* pTrickPlayModule = (C_TrickPlayModule*)
                                C_Application::GetModuleManager()
                                        ->GetModule("trickplay",
                                                    strTrickPlayType);
  if (pTrickPlayModule)
  {
    C_TrickPlayConfig cTrickPlayConfig;
    cTrickPlayConfig.m_hLog = m_hLog;
    cTrickPlayConfig.m_pBroadcast = pBroadcast;
    cTrickPlayConfig.m_pReader = pReader;
    cTrickPlayConfig.m_pHandler = pBuffer;
    cTrickPlayConfig.m_iInitFill = pBuffer->Capacity();
    cTrickPlayConfig.m_pEventHandler = m_pEventHandler;
    cTrickPlayConfig.m_pTsProvider = pTsProvider;
    cTrickPlayConfig.m_pConverter = pConverter;

    pTrickPlay = pTrickPlayModule->NewTrickPlay(cTrickPlayConfig);
    ASSERT(pTrickPlay);
  }
  else
  {
    throw E_Exception(GEN_ERR,
                      "Module TrickPlay:" + strTrickPlayType +
                      " not present");
  }

  // Create the streamer
  C_TsStreamer* pStreamer = new C_TsStreamer(m_hLog, pBroadcast,
                                             pTsProvider, pBuffer,
                                             m_pEventHandler, true, true);
  ASSERT(pStreamer);

  m_cTrickPlay.Add(pBroadcast, pTrickPlay);
  m_cStreamers.Add(pBroadcast, pStreamer);
  m_cConverters.Add(pBroadcast, pConverter);
  try
  {
    pTrickPlay->Create();
    pStreamer->Create();
  }
  catch(E_Exception e)
  {
    pStreamer->Stop();
    pTrickPlay->Stop();

    // Unregister the 2 thread and delete them
    m_cTrickPlay.Delete(pBroadcast);
    m_cStreamers.Delete(pBroadcast);

    throw E_Exception(GEN_ERR, "unable to start streaming of program "+
                      pBroadcast->GetProgram()->GetName(), e);
  }
}


//------------------------------------------------------------------------------
// Resume the reception of the given program
//------------------------------------------------------------------------------
void C_LocalInput::OnResumeStreaming(C_Broadcast* pBroadcast)
{
  ASSERT(pBroadcast);

  // Find the converter that receive the pgrm
  C_TrickPlay* pTrickPlay = m_cTrickPlay.Get(pBroadcast);
  ASSERT(pTrickPlay);

  // Resume the thread
  try
  {
    pTrickPlay->Resume();
  }
  catch(E_Exception e)
  {
    throw E_Exception(GEN_ERR, "Unable to resume streaming of program "+
                      pBroadcast->GetProgram()->GetName(), e);
  }
}


//------------------------------------------------------------------------------
// Suspend the reception of the given program
//------------------------------------------------------------------------------
void C_LocalInput::OnSuspendStreaming(C_Broadcast* pBroadcast)
{
  ASSERT(pBroadcast);

  // Find the converter that receive the pgrm
  C_TrickPlay* pTrickPlay = m_cTrickPlay.Get(pBroadcast);
  ASSERT(pTrickPlay);

  C_String strPgrmName = pBroadcast->GetProgram()->GetName();

  // Suspend the thread
  try
  {
    pTrickPlay->Suspend();
  }
  catch(E_Exception e)
  {
    throw E_Exception(GEN_ERR, "Unable to resume streaming of program "+
                      pBroadcast->GetProgram()->GetName(), e);
  }
}

//------------------------------------------------------------------------------
// Forward the reception of the given program with specified speed
//------------------------------------------------------------------------------
void C_LocalInput::OnForwardStreaming(C_Broadcast* pBroadcast, int speed)
{
  ASSERT(pBroadcast);

  // Find the reader and the streamer that receive the pgrm
  C_TrickPlay* pTrickPlay = m_cTrickPlay.Get(pBroadcast);
  ASSERT(pTrickPlay);

  // Call forward to the threads
  try
  {
    pTrickPlay->Forward(speed);
  }
  catch(E_Exception e)
  {
    throw E_Exception(GEN_ERR, "Unable to forward into the streaming of program "+
                      pBroadcast->GetProgram()->GetName(), e);
  }
}

//------------------------------------------------------------------------------
// Rewind the reception of the given program with specified speed
//------------------------------------------------------------------------------
void C_LocalInput::OnRewindStreaming(C_Broadcast* pBroadcast, int speed)
{
  ASSERT(pBroadcast);

  // Find the reader and the streamer that receive the pgrm
  C_TrickPlay* pTrickPlay = m_cTrickPlay.Get(pBroadcast);
  ASSERT(pTrickPlay);

  // Call rewind to the threads
  try
  {
    pTrickPlay->Rewind(speed);
  }
  catch(E_Exception e)
  {
    throw E_Exception(GEN_ERR, "Unable to rewind into the streaming of program "+
                      pBroadcast->GetProgram()->GetName(), e);
  }
}

//------------------------------------------------------------------------------
// Stop the reception of the given program
//------------------------------------------------------------------------------
void C_LocalInput::OnStopStreaming(C_Broadcast* pBroadcast)
{
  ASSERT(pBroadcast);

  // Find the reader and the streamer that receive the pgrm
  C_TrickPlay* pTrickPlay = m_cTrickPlay.Remove(pBroadcast);
  ASSERT(pTrickPlay);
  C_TsStreamer* pStreamer = m_cStreamers.Remove(pBroadcast);
  ASSERT(pStreamer);
  C_MpegConverter* pConverter = m_cConverters.Remove(pBroadcast);
  ASSERT(pConverter);

  // Stop the threads
  try
  {
    pStreamer->Stop();
    pTrickPlay->Stop();
    delete pTrickPlay;
    delete pConverter;
    delete pStreamer; // streamer MUST be deleted last
  }
  catch(E_Exception e)
  {
    throw E_Exception(GEN_ERR, "Unable to stop streaming of program "+
                      pBroadcast->GetProgram()->GetName(), e);
  }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_LocalInput::OnUpdateProgram(C_String strProgram,
                                     C_String strFileName, C_String strType)
{
  C_Application* pApp = C_Application::GetApp();
  ASSERT(pApp);
  int uiPgrmCount = pApp->GetSetting(GetName()+".programcount", "0").ToInt();


  unsigned int uiId = m_vProgramNames.Find(strProgram) + 1;
  if (uiId>0)
  {
    Log(m_hLog, LOG_WARN, "Program already exists in the list for this input");
  }

  printf("uiPgrmCount =%d\n", uiPgrmCount);
  if(uiPgrmCount)
  {
      uiPgrmCount++;
      C_String strKey = C_String(uiPgrmCount)+ ".name";
      pApp->SetSettings(strKey,strProgram);
      strKey = C_String(uiPgrmCount)+ ".filename";
      pApp->SetSettings(strKey, strFileName);
      strKey = C_String(uiPgrmCount)+ ".type";
      pApp->SetSettings(strKey, strType);
      strKey = C_String("input.programcount");
      C_String strPgrmCount=C_String(uiPgrmCount);
      pApp->SetSettings(strKey, strPgrmCount);

      // add to m_vProgramNames
      C_String strName = pApp->GetSetting(C_String(uiPgrmCount) + ".name",
                                                C_String("Pgrm") + uiPgrmCount);
      C_String* pStr = new C_String(strName);
      m_vProgramNames.Add(pStr);
  }
  else
  {
    throw E_Exception(GEN_ERR, "No files defined in vls.cfg");
  }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_LocalInput::OnDeleteProgram(C_String strProgram)
{
  C_Application* pApp = C_Application::GetApp();
  ASSERT(pApp);
  // Get the type of the program
  unsigned int uiId = m_vProgramNames.Find(strProgram) + 1;
  if (uiId>0)
  {
    // Delete settings from m_cSettings
    C_String strPgrmCount = pApp->GetSetting("input.programcount", "0");
    unsigned int uiPgrmCount = strPgrmCount.ToInt();

    if(uiPgrmCount)
    {
      C_String strDelKey = C_String(uiId)+ ".name";
      pApp->DeleteSetting(strDelKey);
      if (pApp->GetSetting(C_String(uiId)+".filename", "") != "" )
      {
        strDelKey = C_String(uiId)+ ".filename";
        pApp->DeleteSetting(strDelKey);
      }
      if (pApp->GetSetting(C_String(uiId)+".device", "") != "" )
      {
        strDelKey = C_String(uiId)+ ".device";
        pApp->DeleteSetting(strDelKey);
      }
      strDelKey = C_String(uiId)+ ".type";
      pApp->DeleteSetting(strDelKey);

      // Move all other settings one item down
      for(unsigned int ui = uiId; ui <= uiPgrmCount; ui++)
      {
        C_String strProgram = pApp->GetSetting(C_String(ui+1) + ".name",
                                                       C_String("Pgrm") + ui+1);
        C_String strFileName = pApp->GetSetting(C_String(ui+1)+".filename", "");
        C_String strType = pApp->GetSetting(C_String(ui+1)+".type", "mpeg2-ts");
        C_String strDevice = pApp->GetSetting(C_String(ui+1)+".device", "");

        // update settings
        C_String strKey = C_String(ui)+ ".name";
        pApp->SetSettings(strKey,strProgram);
        strKey = C_String(ui)+ ".filename";
        pApp->SetSettings(strKey, strFileName);
        strKey = C_String(ui)+ ".device";
        pApp->SetSettings(strKey, strDevice);
        strKey = C_String(ui)+ ".type";
        pApp->SetSettings(strKey, strType);
      }
      // Lower program count
      uiPgrmCount--;
      strDelKey = C_String("input.programcount");
      C_String strProgramCount=C_String(uiPgrmCount);
      pApp->SetSettings(strDelKey, strPgrmCount);
      //pApp->SetSettings(strDelKey, C_String(uiPgrmCount));
      m_vProgramNames.Delete(uiId);

      // ** DEBUG **
      Log(m_hLog, LOG_NOTE, "Debugging inputs m_cSettings list.");
      C_String strPgrmCount = pApp->GetSetting("input.programcount", "0");
      unsigned int uiPgrmCount = strPgrmCount.ToInt();
      for(unsigned int ui = 1; ui <= uiPgrmCount; ui++)
      {
        C_String strProgram = pApp->GetSetting(C_String(ui) + ".name",C_String("Pgrm") + ui);
        C_String strFileName = pApp->GetSetting(C_String(ui)+".filename", "");
        C_String strDevice = pApp->GetSetting(C_String(ui)+".device", "");
        C_String strType = pApp->GetSetting(C_String(ui) + ".type", "mpeg2-ts").ToLower();
        Log(m_hLog, LOG_NOTE, "Program: \"" + strProgram +"\"");
        Log(m_hLog, LOG_NOTE, "  FileName: \"" + strFileName+"\"");
        Log(m_hLog, LOG_NOTE, "  Device: \"" + strDevice+"\"");
        Log(m_hLog, LOG_NOTE, "  Type: \"" + strType+"\"");
      }
    }
    else
    {
      throw E_Exception(GEN_ERR,"Deleting program information from input failed");
    }
  }
  else
  {
    throw E_Exception(GEN_ERR, "The program is not known by this input.");
  }
}
