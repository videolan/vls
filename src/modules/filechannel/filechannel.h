/*******************************************************************************
* filechannel.h: file channel
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: filechannel.h,v 1.2 2003/08/05 23:18:19 nitrox Exp $
*
* Authors: James Courtier-Dutton <James@superbug.demon.co.uk>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _FILECHANNEL_H_
#define _FILECHANNEL_H_


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_FileChannel : public C_Channel
{
 public:
  C_FileChannel(C_Module* pModule, const C_ChannelConfig& cConfig);
};


DECLARE_MODULE(File, Channel, "file", const C_ChannelConfig&);


#else
#error "Multiple inclusions of filechannel.h"
#endif

