/*******************************************************************************
* dvbreader.cpp: file reader
*-------------------------------------------------------------------------------
* (c)1999-2002 VideoLAN
* $Id: dvbreader.cpp,v 1.8 2003/10/27 10:58:10 sam Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*          Tristan Leteurtre <tooney@videolan.org>
*          Damien Lucas <nitrox@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include <sys/poll.h>
#include <sys/types.h>
#include <fcntl.h>

#include "config.h"

#include "../../core/core.h"

#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"

#include "../../server/buffer.h"
#include "../../server/broadcast.h"
#include "../../server/request.h"

#include "../../mpeg/reader.h"
#include "../../mpeg/converter.h"

#include "dvbreader.h"


//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_DvbMpegReaderModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_dvbreader(handle hLog)
{
  return new C_DvbMpegReaderModule(hLog);
}
#endif


/*******************************************************************************
* C_DvbMpegReader
********************************************************************************
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_DvbMpegReader::C_DvbMpegReader(C_Module* pModule,
                                   C_Broadcast* pBroadcast) :
                        C_MpegReader(pModule, pBroadcast)
{
  m_strDeviceName=pBroadcast->GetOption("device");
  m_bIgnoreTimeout=pBroadcast->GetOption("IgnoreTimeout").ToInt();
}


//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
void C_DvbMpegReader::Init()
{
  m_hFd=open(m_strDeviceName.GetString(), O_NONBLOCK|O_RDONLY);
  ASSERT(m_hFd!=-1);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_DvbMpegReader::Close()
{
  close(m_hFd);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_DvbMpegReader::Read(byte* pBuff, s64 iSize)
{
  struct pollfd pfd[1];
  s64 iRc;

  pfd[0].fd=m_hFd;
  pfd[0].events=POLLIN;

  pBuff[0]=0;
  while(pBuff[0]!=0x47)
  {
    if(poll(pfd, 1, 10000))
    {
      if(pfd[0].revents & POLLIN)
      {
        iRc=read(m_hFd, pBuff, 188);
      }
      else
      {
        if (m_bIgnoreTimeout)
        {
          sleep(1);
        }
        else
        {
          return MPEG_STREAMERROR;
        }
      }
    }
    else
    {
      if (m_bIgnoreTimeout)
      {
        sleep(1);
      }
      else
      {
          Log(m_hLog, LOG_NOTE, "Time out !\n");
          return MPEG_STREAMERROR;
      }
    }
  }
  return iRc;

}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_DvbMpegReader::Seek(s64 iOffset, s64 bStartPos)
{
  return (s64)0;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_DvbMpegReader::Size()
{
  ASSERT(false);
  return 0;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_DvbMpegReader::GetPos()
{
  return 0;
}
