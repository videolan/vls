/*******************************************************************************
* dvbreader.h: file reader
*-------------------------------------------------------------------------------
* (c)1999-2002 VideoLAN
* $Id: dvbreader.h,v 1.7 2003/06/02 19:50:35 jpsaman Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*          Tristan Leteutre <tooney@videolan.org>
*          Damien Lucas <nitrox@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Implementation of a dvb stream reader (C_DvbMpegReader).
*******************************************************************************/


#ifndef _DVB_READER_H_
#define _DVB_READER_H_


//------------------------------------------------------------------------------
// C_DvbFileReader class
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_DvbMpegReader : public C_MpegReader 
{
public:
  C_DvbMpegReader(C_Module* pModule, C_Broadcast* pBroadcast);

  virtual void Init();
  virtual void Close();

  virtual s64 Read(byte* pBuff, s64 iSize);
  virtual s64 Seek(s64 iOffset, s64 bStartPos);
  virtual int GetFrame(byte ** pBuff ,int iSize) { return 0;};
  virtual int GetAudioFD() { return 0; }
  
  virtual s64 Size();
  virtual s64 GetPos();

protected:
  C_String m_strDeviceName;
  int m_hFd;

  bool m_bIgnoreTimeout;
  bool m_bLoop;
  bool m_bEnd;
};


// Declaration and implementation of C_FileMpegReaderModule
DECLARE_MODULE(Dvb, MpegReader, "dvb", C_Broadcast*);


#else
#error "Multiple inclusions of dvbreader.h"
#endif

