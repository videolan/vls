/*******************************************************************************
* dummyinterface.cpp:
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: dummyinterface.cpp,v 1.6 2003/10/27 10:58:10 sam Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*          Tristan Leteurtre <tristan.leteurtre@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include <ctype.h>

#include "../../core/core.h"
#include "../../core/network.h"
#include "../../server/request.h"
#include "../../server/repository.h"
#include "../../server/admin.h"
#include "../../server/interface.h"
#include "dummyinterface.h"

#include "../../core/network.cpp"

#include "../../server/repository.cpp"

//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_DummyInterfaceModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_dummyinterface(handle hLog)
{
  return new C_DummyInterfaceModule(hLog);
}
#endif



/*******************************************************************************
* E_DummyInterface exception
********************************************************************************
*
*******************************************************************************/
E_DummyInterface::E_DummyInterface(const C_String& strMsg) :
                      E_Exception(GEN_ERR, strMsg)
{
}

E_DummyInterface::E_DummyInterface(const C_String& strMsg, const E_Exception& e) :
                      E_Exception(GEN_ERR, strMsg, e)
{
}


/*******************************************************************************
* C_DummyInterfaceSession class
********************************************************************************
*
*******************************************************************************/

/*******************************************************************************
* C_DummyInterface class
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_DummyInterface::C_DummyInterface(C_Module* pModule,
                                     C_InterfaceConfig& cConfig) :
               C_Interface(pModule,cConfig)
{
    m_bStop=0;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
int C_DummyInterface::Init()
{
    int iRc = NO_ERR;
	return iRc;
}


void C_DummyInterface::InitSession()
{
}

//------------------------------------------------------------------------------
// Execution
//------------------------------------------------------------------------------
void C_DummyInterface::RunSession()
{
  while(!m_bStop)
  {
    sleep(2);
  }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_DummyInterface::StopSession()
{
    m_bStop=true;
}

//------------------------------------------------------------------------------
// Destruction
//------------------------------------------------------------------------------
void C_DummyInterface::DestroySession()
{
}


