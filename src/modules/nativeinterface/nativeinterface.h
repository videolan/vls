/*******************************************************************************
* nativeinterface.h:
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: nativeinterface.h,v 1.4 2003/07/31 14:46:36 nitrox Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*          Tristan Leteurtre <tristan.leteurtre@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _TELNETINTERFACE_H_
#define _TELNETINTERFACE_H_

///------------------------------------------------------------------------------
// Native admin module definitions
//------------------------------------------------------------------------------
#define INIT_PHASE              1
#define LOGIN_PHASE             2
#define COMMAND_PHASE           4


//------------------------------------------------------------------------------
// E_NativeInterface class
//------------------------------------------------------------------------------
class E_NativeInterface : public E_Exception
{
 public:
  E_NativeInterface(const C_String& strMsg);
  E_NativeInterface(const C_String& strMsg, const E_Exception& e);
};


//------------------------------------------------------------------------------
// C_NativeInterfaceSession class
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_NativeInterfaceSession : public C_InterfaceSession
{
public:
  C_NativeInterfaceSession(C_Socket* pConnection, void* pAdmin);

  // Data processing
  virtual void ProcessData();
  virtual void SendEvent(const C_Event& cEvent);
  C_String m_strPeerName;

protected:

  void HandleMessage(void);
private:
  // Connection to the peer
  //C_Socket* m_pConnection;
  //C_Stream<C_Socket> m_cStream;

  // Session status
  //int m_iPhase;

  // Internal data
  C_String m_strMessage;
};



//------------------------------------------------------------------------------
// C_NativeInterface class
//------------------------------------------------------------------------------
class C_NativeInterface :   public C_Interface,
                            protected C_ConnectionsHandler<C_NativeInterfaceSession>
{
 public:
  C_NativeInterface(C_Module* pModule, C_InterfaceConfig& cConfig);

  void PropagateEvent(const C_Event& cEvent);

  int Init();
  int Run();
  int Stop();
  int Destroy();

 protected:

  virtual void InitSession();
  virtual void StopSession();
  virtual void RunSession();
  virtual void DestroySession();


//private:
//  // Helpers
//  handle m_hLog;
//  C_Admin* m_pAdmin;
//
//  // Event fifo
//  C_Vector<C_Event> m_cEvents;
//  C_Mutex m_cEventsLock;
};


DECLARE_MODULE(Native, Interface, "native", C_InterfaceConfig&);

#else
#error "Multiple inclusions of telnetadmin.h"
#endif

