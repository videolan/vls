/*******************************************************************************
* nativeinterface.cpp:
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: nativeinterface.cpp,v 1.8 2003/10/27 10:58:10 sam Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*          Tristan Leteurtre <tristan.leteurtre@videolan.org>
*          Damien Lucas <damien.lucas@anevia.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include <ctype.h>

#include "../../core/core.h"
#include "../../core/network.h"
#include "../../server/request.h"
#include "../../server/repository.h"
#include "../../server/admin.h"
#include "../../server/interface.h"
#include "nativeinterface.h"

#include "../../core/network.cpp"

#include "../../server/repository.cpp"

#define SENDANSWER(s)   \
    do                  \
    {                   \
      char slen[3];   \
      sprintf(slen, "%04x", s.Length()); \
      m_cStream << C_String(slen)+C_String(" ")+ s + "\n";\
    } while(0)

//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_NativeInterfaceModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_nativeinterface(handle hLog)
{
  return new C_NativeInterfaceModule(hLog);
}
#endif



/*******************************************************************************
* E_NativeInterface exception
********************************************************************************
*
*******************************************************************************/
E_NativeInterface::E_NativeInterface(const C_String& strMsg) :
                      E_Exception(GEN_ERR, strMsg)
{
}

E_NativeInterface::E_NativeInterface(const C_String& strMsg, const E_Exception& e) :
                      E_Exception(GEN_ERR, strMsg, e)
{
}


/*******************************************************************************
* C_NativeInterfaceSession class
********************************************************************************
*
*******************************************************************************/
//-----------------------------------------------------------------------------
// Constructor
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
C_NativeInterfaceSession::C_NativeInterfaceSession(C_Socket* pConnection,
                                                   void* pAdmin) :
                      C_InterfaceSession(pConnection,pAdmin),
                                 m_strPeerName("[Unknown]")
{
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_NativeInterfaceSession::HandleMessage()
{
  unsigned int i;
  if((i =  m_strMessage.Length()) > 5)
             m_strMessage = m_strMessage.SubString(5, i);
  C_Message cMessage(m_strMessage);
  s32 iType = cMessage.GetType();
  if(iType != INVALID_TYPE)
  {
    if(iType == REQUEST_TYPE)
    {
      C_Request cRequest = cMessage.GetRequest();
      if(m_iPhase == LOGIN_PHASE)
      {
        C_Answer cAnswer("NativeInterface");

        if(cRequest.GetCmd() == "login")
        {
          C_String strLogin = cRequest.GetArg("login");
          C_String strPasswd = cRequest.GetArg("password");

          int iRc = Authenticate(strLogin, strPasswd);

          if(!iRc)
          {
            cAnswer.SetStatus(ANS_AUTH_SUCCESS);
            cAnswer.AddMessage("Authentication succeeded");
            m_iPhase = COMMAND_PHASE;
          }
          else
          {
            cAnswer.SetStatus(ANS_AUTH_FAILED);
            cAnswer.AddMessage("Authentication failed");
            m_iPhase = LOGIN_PHASE;
          }
        }
        else
        {
          cAnswer.SetStatus(ANS_AUTH_FAILED);
          cAnswer.AddMessage("Not authenticated");
        }

        C_Message cMessageAnswer(cAnswer);
        SENDANSWER(cMessageAnswer.GetString());
      }
      else if(m_iPhase == COMMAND_PHASE)
      {
        C_Answer cAdminAnswer = m_pAdmin->ValidateRequest(this, cRequest);
        switch(cAdminAnswer.GetStatus())
        {
        case ADMIN_WELLFORMED_COMMAND:
          if(cRequest.GetCmd() == "logout")
          {
            throw E_NativeInterface("logout requested");
          }
          else
          {
            C_Answer cAnswer = m_pAdmin->HandleRequest(cRequest);
            C_Message cMessageAnswer(cAnswer);
            SENDANSWER(cMessageAnswer.GetString());
          }
          break;
        case ADMIN_COMMAND_NOT_VALID:
        case ADMIN_UNKNOWN_COMMAND:
        case ADMIN_COMMAND_DENIED:
          {
            C_Message cMessageAnswer(cAdminAnswer);
            SENDANSWER(cMessageAnswer.GetString());
          }
          break;
        case ADMIN_EMPTY_COMMAND:
          {
            C_Answer cAnswer("");
            C_Message cMessageAnswer(cAnswer);
            SENDANSWER(cMessageAnswer.GetString());
          }
          break;
        default:
          ASSERT(false);
          break;
        }
      }
      else
      {
        ASSERT(false);
      }
    }
    else if(iType == ANSWER_TYPE)
      printf("shouldn't receive any answer => trash\n");
    else /*if(iType == EVENT_TYPE)*/
      printf("shouldn't receive any event => trash\n");
  }
  else
    printf("Invalid message => trash\n");

  m_strMessage = "";
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_NativeInterfaceSession::ProcessData()
{
  try
  {
    // First get the data from the network
    C_Buffer<byte> cData(256);
    m_cStream >> cData;
    if(cData.GetSize() <= 0)
      throw E_NativeInterface("Connection reset by peer");

    unsigned int iPos = 0;
    unsigned int iRef = 0;
    unsigned int iSize = cData.GetSize();

    while(iPos < iSize)
    {
      if(cData[iPos] == '\r' || cData[iPos] == '\n')
      {
        char msg[iPos-iRef+1];
        memcpy(msg, &cData[iRef], iPos-iRef);
        msg[iPos-iRef] = '\0';
        m_strMessage += msg;
        HandleMessage();
        iRef = iPos + 1;
      }

      // Go to next char in input
      iPos++;
    }
    char msg[iPos-iRef+1];
    memcpy(msg, &cData[iRef], iPos-iRef);
    msg[iPos-iRef] = '\0';
    m_strMessage += msg;
  }
  catch(E_Stream<C_Socket> e)
  {
    throw E_NativeInterface("Connection error", e);
  }
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_NativeInterfaceSession::SendEvent(const C_Event& cEvent)
{
    try
    {
        C_Message cMessage(cEvent);
        C_String strEvent = cMessage.GetString();
        if(m_cStream.GetState() == STREAM_READY)
        {
            SENDANSWER(strEvent);
        }
    }
    catch(E_Exception e)
    {
        C_String strPeer = m_pConnection->GetPeerName();
        throw E_Interface("Unableto send data to "+ strPeer, e);
    }
}
/*******************************************************************************
* C_NativeInterface class
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_NativeInterface::C_NativeInterface(C_Module* pModule,
                                     C_InterfaceConfig& cConfig) :
               C_Interface(pModule,cConfig),
         C_ConnectionsHandler<C_NativeInterfaceSession>(cConfig.hLog, cConfig.pAdmin)
{
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_NativeInterface::PropagateEvent(const C_Event& cEvent)
{
//  m_cEventsLock.Lock();

  // Push the event in the fifo
//  C_Event* pEvent = new C_Event(cEvent);
//  m_cEvents.Add(pEvent);

  // Purge the event fifo
//  while(m_cEvents.Size() >= 1)
//  {
    // get the event
//    C_Event* pEvent = m_cEvents.Remove(0);
//    ASSERT(pEvent);

    // send the event to all the sessions
//    C_HashTable<handle, Session> m_cSessions;

    C_HashTableIterator<handle, C_NativeInterfaceSession> cIterator =
                                            m_cSessions.CreateIterator();
    while(cIterator.HasNext())
    {
      C_HashTableNode<handle, C_NativeInterfaceSession>* pNode =
                                            cIterator.GetNext();
      C_NativeInterfaceSession* pSession = pNode->GetValue();
      pSession->SendEvent(cEvent);
    }

//    delete pEvent;
//  }

//  m_cEventsLock.UnLock();

//  m_cEventsLock.UnLock();
}


//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
int C_NativeInterface::Init()
{
  C_Application* pApp = C_Application::GetApp();
  ASSERT(pApp);

  int iRc = NO_ERR;

  // Get config
  C_String strDomain = pApp->GetSetting("Native.Domain", "Inet4").ToLower();

  int iDomain;
  C_String strDefaultHost;

  if(strDomain == "inet4")
  {
    iDomain = AF_INET;
    strDefaultHost = "0.0.0.0";
  }
#ifdef HAVE_IPV6
  else if(strDomain == "inet6")
  {
    iDomain = AF_INET6;
    strDefaultHost = "0::0";
  }
#endif
  else
  {
    Log(C_Interface::m_hLog, LOG_ERROR, "Unknown domain:\n" + strDomain);
    iRc = GEN_ERR;
  }

  if(!iRc)
  {
    C_String strAddr = pApp->GetSetting("NativeInterface.LocalAddress",
                                        strDefaultHost);
    C_String strPort = pApp->GetSetting("NativeInterface.LocalPort", "9998");

    try
    {
      C_ConnectionsHandler<C_NativeInterfaceSession>::Init(iDomain,
                                                       strAddr, strPort);
    }
    catch(E_Exception e)
    {
      Log(C_Interface::m_hLog, LOG_ERROR,
          "Native administrator initialisation failed:\n" + e.Dump());
      iRc = GEN_ERR;
    }
  }

  if(!iRc)
    Log(C_Interface::m_hLog, LOG_NOTE, "Native administrator initialised");

  return iRc;
}


void C_NativeInterface::InitSession()
{
}

//------------------------------------------------------------------------------
// Execution
//------------------------------------------------------------------------------
void C_NativeInterface::RunSession()
{
    C_ConnectionsHandler<C_NativeInterfaceSession>::Run();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_NativeInterface::StopSession()
{
    C_ConnectionsHandler<C_NativeInterfaceSession>::Stop();
}

//------------------------------------------------------------------------------
// Destruction
//------------------------------------------------------------------------------
void C_NativeInterface::DestroySession()
{
    C_ConnectionsHandler<C_NativeInterfaceSession>::Destroy();
}


