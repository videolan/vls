/*******************************************************************************
* normaltrickplay.cpp: The normal implementation from trickplay. It only supports
*                      start, stop, suspend and resume operations.
*-------------------------------------------------------------------------------
* (c)1999-2003 VideoLAN
* $Id: normaltrickplay.cpp,v 1.16 2003/10/27 10:58:11 sam Exp $
*
* Authors: Jean-Paul Saman <jpsaman@wxs.nl>          
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../../core/core.h"

#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"
#include "../../mpeg/rtp.h"
#include "../../server/buffer.h"
#include "../../server/program.h"
#include "../../server/output.h"
#include "../../server/channel.h"
#include "../../server/broadcast.h"
#include "../../server/request.h"
#include "../../server/input.h"

#include "../../mpeg/reader.h"
#include "../../mpeg/converter.h"
#include "../../mpeg/trickplay.h"

#include "normaltrickplay.h"


//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_NormalTrickPlayModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_normaltrickplay(handle hLog)
{
  return new C_NormalTrickPlayModule(hLog);
}
#endif


/*******************************************************************************
* C_NormalTrickPlayModule
********************************************************************************
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_NormalTrickPlay::C_NormalTrickPlay(C_Module* pModule,
                               C_TrickPlayConfig& cConfig)
: C_TrickPlay(pModule,cConfig)
{
}


//------------------------------------------------------------------------------
// Main job
//------------------------------------------------------------------------------

void C_NormalTrickPlay::HandlePacket(C_TsPacket* pPacket)
{
  if (!m_pHandler->HandlePacket(pPacket)) {
    m_pTsProvider->ReleasePacket(pPacket);
  }
}

void C_NormalTrickPlay::DoWork()
{
  C_TsPacket * pPacket;
  int iRc = NO_ERR;

  // Initialize the condition in the new thread.
  m_cResumeCond.Protect();

  // log it
  C_String strPgrmName = m_pBroadcast->GetProgram()->GetName();
  LogDbg(m_hLog, "Starting to read program \"" + strPgrmName + "\"");


  // the main loop
  unsigned int prefillCount = 0;
  while(!m_bStop && !iRc)
  {

    if (m_iTrickPlayStatus != m_iRequestTrickPlayStatus)
    {
      switch (m_iRequestTrickPlayStatus)
      {
        case TRICKPLAY_PLAY:
          // implement your own policy here
          // ..
          m_iTrickPlayStatus = TRICKPLAY_PLAY;
          break;
        case TRICKPLAY_PAUSE:
          // implement your own policy here
          // ..
          m_iTrickPlayStatus = TRICKPLAY_PAUSE;
          m_cResumeCond.Wait();
          break;
        case TRICKPLAY_FORWARD: // Not implemented yet
          m_iTrickPlayStatus = TRICKPLAY_PLAY;
          m_iRequestTrickPlayStatus = TRICKPLAY_PLAY;
          break;
        case TRICKPLAY_REWIND: // Not implemented yet
          m_iTrickPlayStatus = TRICKPLAY_PLAY;
          m_iRequestTrickPlayStatus = TRICKPLAY_PLAY;
          break;
        case TRICKPLAY_STOP:
          m_iTrickPlayStatus = TRICKPLAY_STOP;
          m_bStop = true;
          break;
        default:
          LogDbg(m_hLog, "Unknown TrickPlay mode requested.");
          m_iRequestTrickPlayStatus = m_iTrickPlayStatus;
          break;
      }
    }

    iRc = m_pConverter->GetNextTsPackets(m_pPackets);

    int iVectorSize = m_pPackets->Size();

    if ((iRc == NO_ERR) && (!m_bStop))
    {
      for(int i = 0; i < iVectorSize;  i++)
      {
        pPacket =  m_pPackets->Pop();
        ASSERT(pPacket);
        if (prefillCount >= m_iInitFill) {
          HandlePacket(pPacket);
        } else {
          if (!m_pHandler->HandlePrefillPacket(pPacket)) {
            HandlePacket(pPacket);
            m_pHandler->PrefillComplete();
            prefillCount = m_iInitFill+1; // make sure we don't do any more prefilling
          } else if (++prefillCount >= m_iInitFill) {
            m_pHandler->PrefillComplete();
          }
        }
      }
    }
    else
    {
      while(m_pPackets->Size() > 0) {
        pPacket = m_pPackets->Pop();
        ASSERT(pPacket);
        if (pPacket->RefCount()>0)
          m_pTsProvider->ReleasePacket(pPacket);
      }
    }
  }

  if(!m_bStop)
  {
    LogDbg(m_hLog, "Stopping converter by callback for program " + strPgrmName);
    m_bStop = true;
    C_Event cEvent(m_pBroadcast->GetInput()->GetName());
    cEvent.SetCode(EOF_EVENT);
    cEvent.SetBroadcast(m_pBroadcast);
    m_pEventHandler->HandleEvent(cEvent);
  }

  m_pHandler->PrefillComplete();
  delete m_pPackets;

  m_cResumeCond.Release();
  LogDbg(m_hLog, "Converter stopped for program " + strPgrmName);
}

void C_NormalTrickPlay::StopWork()
{
  // TODO
  // Should test the resumecond state before releasing it
  // debuggers like valgrind really don't like that
  // nitrox
  m_cResumeCond.Release();
  C_TrickPlay::StopWork();
  m_pHandler->Shutdown();
}

void C_NormalTrickPlay::InitWork()
{
  C_TrickPlay::InitWork();

  if (m_iInitFill < 1)
    m_iInitFill = 1;

  // Fill the buffer
  m_pPackets = new C_Fifo<C_TsPacket>(700);
  m_pHandler->PrefillStart();
}

void C_NormalTrickPlay::CleanWork()
{
  m_pConverter->CleanWork();
}

// TrickPlay functionality
void C_NormalTrickPlay::Resume()
{
  C_TrickPlay::Resume();
  if (m_iRequestTrickPlayStatus == TRICKPLAY_PLAY)
  {
    m_cResumeCond.Protect();
    m_cResumeCond.Signal();
  }
}

void C_NormalTrickPlay::Suspend()
{
  C_TrickPlay::Suspend();    
}

void C_NormalTrickPlay::Forward(int speed)
{
  C_TrickPlay::Forward(speed);    
}

void C_NormalTrickPlay::Rewind(int speed)
{
  C_TrickPlay::Rewind(speed);    
}
