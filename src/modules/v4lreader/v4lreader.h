/*******************************************************************************
* v4lreader.h: eading from a v4l device (WebCam, tv tuner)
*-------------------------------------------------------------------------------
* (c)1999-2003 VideoLAN
* $Id: v4lreader.h,v 1.12 2003/10/16 10:49:51 tooney Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*          Tristan Leteutre <tooney@videolan.org>
*          Damien Lucas <nitrox@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Implementation of a v4l stream reader (C_v4lMpegReader).
*******************************************************************************/


#ifndef _V4L_READER_H_
#define _V4L_READER_H_

#include <linux/videodev.h>

class E_v4l : public E_Exception
{
public:
  E_v4l(const C_String& strMsg);
};

//------------------------------------------------------------------------------
// C_v4lMpegReader class
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_v4lMpegReader : public C_MpegReader
{
public:
  C_v4lMpegReader(C_Module* pModule, C_Broadcast* pBroadcast);

  virtual void Init();
  virtual void Close();

  virtual s64 Read(byte* pBuff, s64 iSize);
  virtual s64 Seek(s64 iOffset, s64 bStartPos);
  virtual s64 Size();
  virtual s64 GetPos();

  virtual int GetFrame(byte ** pBuff ,int iSize);
  virtual int GetAudioFD();
  
protected:
  C_Broadcast* m_pBroadcast;
  C_String m_strDeviceName;
  int m_hFd;

  bool m_bLoop;
  bool m_bEnd;
  bool m_bUseMmap;

  // Video For Linux variables, used for ioctls
  struct video_mmap m_Vmmap;
  struct video_picture m_Vpic;
  struct video_mbuf m_Vbuf;
  struct video_capability m_Vcap;
  struct video_window m_Vwin;
  struct video_audio m_Audio, m_Audio_saved;
  struct video_channel m_Vchan;

  // Pointer to the video buffer from mmap
  byte * m_pVideoMap;

  // Grab frame
  int m_iCurrentFrame;

  // Video4Linux parameters
  int m_iCurrentChannel;
  int m_iNorm;
  int m_iFrequency;
  int m_iSample_rate;
  int m_iChannel;
  
  // parameters of the image
  int m_iHeight, m_iWidth, m_iFrameRate;

  byte * m_pPictureBuffer; // Pointer to the picture buffer

  // variables for dsp reading
  bool m_bMute;
  C_String m_strAudioDeviceName;
  int m_iAudioFD; // file descriptor
  byte * m_pAudioBuffer;
};


// Declaration and implementation of C_FileMpegReaderModule
DECLARE_MODULE(v4l, MpegReader, "v4l", C_Broadcast*);


#else
#error "Multiple inclusions of v4lreader.h"
#endif

