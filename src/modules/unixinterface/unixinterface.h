/*******************************************************************************
* unixinterface.h: Unix class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: unixinterface.h,v 1.1 2003/10/30 20:53:53 tooney Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*          Tristan Leteurtre <tristan.leteurtre@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _UNIXINTERFACE_H_
#define _UNIXINTERFACE_H_

//------------------------------------------------------------------------------
// E_Unix class
//------------------------------------------------------------------------------
class E_UnixInterface : public E_Exception
{
 public:
  E_UnixInterface(const C_String& strMsg);
  E_UnixInterface(const C_String& strMsg, const E_Exception& e);
};

//------------------------------------------------------------------------------
// C_UnixInterfaceSession class
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_UnixInterfaceSession : public C_InterfaceSession
{
 public:
  C_UnixInterfaceSession(C_Socket* pConnection, void* pAdmin);

  // Data processing
  virtual void ProcessData();

  C_String m_strPeerName;

 protected:
  virtual void OnInit();
  virtual void OnClose();

  void HandleByte(byte bChar);
  void HandleLine();

  void SendAnswer(const C_Answer& cAnswer, const C_String& strPrefix = "");

 private:

  // Session status
  int m_iMode;

  // Internal data
  C_String m_strPath;
  C_String m_strPrompt;
  C_String m_strLogin;
    
  C_String m_strCmd;
  C_Buffer<byte> m_cBuffer;
};



//------------------------------------------------------------------------------
// C_UnixInterface class
//------------------------------------------------------------------------------
class C_UnixInterface : public C_Interface,
                          protected C_ConnectionsHandler<C_UnixInterfaceSession>
{
 public:
  C_UnixInterface(C_Module* pModule, C_InterfaceConfig& cConfig);

  int Init();
  int Run();
  int Stop();
  int Destroy();

 protected:

  virtual void InitSession();
  virtual void StopSession();
  virtual void RunSession();
  virtual void DestroySession();

  
// private:
  // Helpers
//  handle m_hLog;
//  C_Admin* m_pAdmin;

};


DECLARE_MODULE(Unix, Interface, "unix", C_InterfaceConfig&);

#else
#error "Multiple inclusions of unixadmin.h"
#endif

