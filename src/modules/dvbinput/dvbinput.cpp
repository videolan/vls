/*******************************************************************************
* dvbinput.cpp: DVB streams
*-------------------------------------------------------------------------------
* (c)1999-2002 VideoLAN
* $Id$
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*          Damien Lucas <nitrox@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"
#include "../../core/core.h"

#include <fcntl.h>

#ifdef HAVE_DVBPSI_DVBPSI_H
#   include <dvbpsi/dvbpsi.h>
#   include <dvbpsi/descriptor.h>
#   include <dvbpsi/pat.h>
#   include <dvbpsi/pmt.h>
#else
#   include "src/dvbpsi.h"
#   include "src/descriptor.h"
#   include "src/tables/pat.h"
#   include "src/tables/pmt.h"
#endif



// TODO nitrox
// We should be able to get rid of those headers
// using lidvb (the unselectPID function is the only
// one that lacks in last version (4.1) )
#include <sys/ioctl.h>
#include <linux/dvb/frontend.h>
#include <linux/dvb/dmx.h>

// TODO nitrox
// This should not be a system include since libdvb is not
// yet package on all systems
// See configure.in
#include <DVB.hh>

#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"
#include "../../mpeg/rtp.h"

#include "../../server/program.h"
#include "../../server/buffer.h"
#include "../../server/output.h"
#include "../../server/channel.h"
#include "../../server/broadcast.h"
#include "../../server/request.h"
#include "../../server/input.h"
#include "../../server/tsstreamer.h"

#include "../../mpeg/reader.h"
#include "../../mpeg/trickplay.h"
#include "../../mpeg/converter.h"
#include "../../mpeg/tsdemux.h"
#include "../../mpeg/dvbpsi.h"
#include "../../mpeg/tsmux.h"

#include "dvbinput.h"

#define FILELEN 256

//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_DvbInputModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_dvbinput(handle hLog)
{
  return new C_DvbInputModule(hLog);
}
#endif


/*******************************************************************************
* C_DvbInput class
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_DvbInput::C_DvbInput(C_Module* pModule,
                       const C_String& strName) :
                                C_Input(pModule, strName),
                                C_TsDemux(&m_cTsProvider),
                                m_cTsProvider(500),
                                m_cInputProgram(/*0,*/ "Input DVB " + strName),
                                m_cInputBroadcast(&m_cInputProgram, this, NULL, strName),
                                m_cPatDecoder(&m_cTsProvider, this),
                                m_cCurrentPat(0, 0, true)
{
  dvb = new DVB;
  m_iGotTpid = 0;            // Did not set the transponder yet
  m_iDemuxUsageCount = 0;    // Nothing using the demux yet
  m_bIgnoreTimeout = false;
  m_bIgnoreMissing = false;
  m_pConverter = NULL;
  m_pTrickPlay = NULL;
  for(int i =0; i < 512; i++)
    m_iDemuxes[i] = -1;
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_DvbInput::~C_DvbInput()
{
}


//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
void C_DvbInput::OnInit()
{
  int iNumber;
  int iMinor;
  C_String strType;
  C_String dvbrc;
  char filen[FILELEN];
  char devname[80];

  // Retrieve config
  C_Application* pApp = C_Application::GetApp();
  ASSERT(pApp);

  iNumber = pApp->GetSetting(GetName() + ".devicenumber", "0").ToInt();
  iMinor = pApp->GetSetting(GetName() + ".minor", "0").ToInt();
  m_strTrickPlayType = pApp->GetSetting(GetName() + ".trickplay", "Normal").ToLower();
  m_iSendMethod = pApp->GetSetting(GetName() + ".sendmethod", "0").ToInt();
  m_bIgnoreTimeout = pApp->GetSetting(GetName()+".ignoretimeout", "0").ToInt();
  m_bIgnoreMissing = pApp->GetSetting(GetName()+".ignoremissing", "0").ToInt();
  dvbrc = pApp->GetSetting(GetName()+ ".dvbrc", "");
  if(dvbrc.Length() != 0)
  {
    strncpy(filen, dvbrc.GetString(), dvbrc.Length()+1);
  }
  dvb->init("", "", iNumber, iMinor);

  sprintf(devname, DVR_DEV,   iNumber, 0); m_strDVR   = C_String(devname);
  sprintf(devname, DEMUX_DEV, iNumber, 0); m_strDEMUX = C_String(devname);
  sprintf(devname, VIDEO_DEV, iNumber, 0); m_strVIDEO = C_String(devname);

  //Check whether card has a decoder:
  //When opening the video device, we should get an error
  int iDummy=open(m_strVIDEO.GetString(), O_RDWR|O_NONBLOCK);
  m_bHasDecoder=(iDummy<0) ? false : true;
  close(iDummy);
  switch(dvb->front_type)
  { 
    case FE_QPSK:
      strType = "DVB-S";
      break;
    case FE_QAM:
      strType= "DVB-C";
      break;
    case FE_OFDM:
      strType = "DVB-T";
      break;
    default:
      throw E_Exception(GEN_ERR, "No DVB card found");
      break;
  }
  Log(m_hLog, LOG_NOTE, strType + " Card registered "+
                  (m_bHasDecoder ? "with decoder\n" : "without decoder\n"));

  //Get the .dvbrc file
  if (!get_dvbrc(filen,*dvb,iNumber,FILELEN))
    throw E_Exception(GEN_ERR, "Unable to find any dvbrc file");

  for(int i=0; i<dvb->NumChannel(); i++)
  {
    C_String* pStr=new C_String(dvb->chans[i].name);
    /* Let's replace all spaces by '_' */
    pStr->Replace(' ', '_');
    m_vProgramNames.Add(pStr);
    Log(m_hLog, LOG_NOTE, "Added program '" + *pStr+"'");
  }

  // PAT decoder initialization
  m_cPatDecoder.Attach();

  // Reader
  C_MpegReaderModule* pReaderModule = (C_MpegReaderModule*)
                                C_Application::GetModuleManager()
                                        ->GetModule("mpegreader",
                                                    "dvb");
  ASSERT(pReaderModule);

  m_cInputBroadcast.SetOption("device", m_strDVR);
  m_cInputBroadcast.SetOption("IgnoreTimeout", m_bIgnoreTimeout);
  m_pReader = pReaderModule->NewMpegReader(&m_cInputBroadcast);
  ASSERT(m_pReader);

  // Converter
  C_MpegConverterModule* pConverterModule = (C_MpegConverterModule*)
                                C_Application::GetModuleManager()
                                        ->GetModule("mpegconverter",
                                                    "ts2ts");
  ASSERT(pConverterModule);

  C_MpegConverterConfig cConfig;
  cConfig.m_hLog = m_hLog;
  cConfig.m_pBroadcast = &m_cInputBroadcast;
  cConfig.m_pReader = m_pReader;
  cConfig.m_pTsProvider = m_pTsProvider;
  cConfig.m_pEventHandler = this;
  m_pConverter = pConverterModule->NewMpegConverter(cConfig);
  ASSERT(m_pConverter);

  // Create the TrickPlay
  C_TrickPlayModule* pTrickPlayModule = (C_TrickPlayModule*)
                                C_Application::GetModuleManager()
                                        ->GetModule("trickplay",
                                                    m_strTrickPlayType);
  if (pTrickPlayModule)
  {
    C_TrickPlayConfig cTrickPlayConfig;
    cTrickPlayConfig.m_hLog = m_hLog;
    cTrickPlayConfig.m_pBroadcast = &m_cInputBroadcast;
    cTrickPlayConfig.m_pReader = m_pReader;
    cTrickPlayConfig.m_pEventHandler = this;
    cTrickPlayConfig.m_pTsProvider = m_pTsProvider;
    cTrickPlayConfig.m_pConverter = m_pConverter;
    cTrickPlayConfig.m_pHandler = this;
    cTrickPlayConfig.m_iInitFill = 0;
    m_pTrickPlay = pTrickPlayModule->NewTrickPlay(cTrickPlayConfig);
    ASSERT(m_pTrickPlay);
  }
  else
  {
    throw E_Exception(GEN_ERR, "Module TrickPlay:" + m_strTrickPlayType +
                      " not present");
  }
}


//------------------------------------------------------------------------------
// Destruction
//------------------------------------------------------------------------------
void C_DvbInput::OnDestroy()
{

  // PAT Decoder destruction
  m_cPatDecoder.Detach();

  try
  {
    if(m_pConverter)
    {
      delete m_pConverter;
    }
    if(m_pTrickPlay)
    {
      if(m_pTrickPlay->IsRunning())
        m_pTrickPlay->Stop();
      delete m_pTrickPlay;
    }
  }
  catch(E_Exception e)
  {
    m_cEndInit.Release();
    if (m_pConverter) delete m_pConverter;
    if (m_pTrickPlay) delete m_pTrickPlay;
    throw e;
  }

  m_cEndInit.Release();
}


//------------------------------------------------------------------------------
// Return the programs read in the dvbrc file
//------------------------------------------------------------------------------
C_List<C_Program> C_DvbInput::OnGetAvailablePgrms()
{
  C_List<C_Program> cPgrmList;

  m_cLock.Lock();
  for(int i=0; i<dvb->NumChannel(); i++)
  { 
    C_String pStr=dvb->chans[i].name;
    /* Let's replace all spaces by '_' */
    pStr.Replace(' ', '_');
    C_Program* pProgram = new C_Program(pStr, dvb->chans[i].pnr);
    ASSERT(pProgram);
    cPgrmList.PushEnd(pProgram);
  }

  m_cLock.UnLock();

  return cPgrmList;
}


//------------------------------------------------------------------------------
// Hardware PID selection
//------------------------------------------------------------------------------
void C_DvbInput::OnSelectPid(u16 iPid, u8 iType)
{
  int i;
  for(i = 0; (i < 256) && (m_iDemuxes[2 * i] != -1); i++);

  if(i < 256)
  {
    // We don't always need to send PES unknown by vlc
    // Thx to Marian Durkovic
    // but we may want to have private datas like subtitles
    if((m_iSendMethod == 0) || (iType < TS_TYPE_MPEG2_PRIVATE)) 
    {

      int iFd = open(m_strDEMUX.GetString(), O_RDWR|O_NONBLOCK);
      if(iFd < 0)
        Log(m_hLog, LOG_ERROR, "Unable to open demux");
      else

      {
        struct dmx_pes_filter_params pesFilterParams;

        pesFilterParams.pid     = iPid;
        pesFilterParams.input   = DMX_IN_FRONTEND;
        pesFilterParams.output  = DMX_OUT_TS_TAP;

        // With Nova Cards, putting DMX_PES_VIDEO & DMX_PES_AUDIO
        // doesn't work when receiving severals programs
        // Putting DMX_PES_OTHER everywhere seems to work...
        //
        // But, with DVB-S Cards, you should put the exact type ...
        // It implicates that can receive only 1 audio channel

        if(!m_bHasDecoder) pesFilterParams.pes_type = DMX_PES_OTHER;
        else
        {
          switch(iType)
          {
            case TS_TYPE_MPEG1_VIDEO:
            case TS_TYPE_MPEG2_VIDEO:
               pesFilterParams.pes_type = DMX_PES_VIDEO;
               break;
            case TS_TYPE_MPEG1_AUDIO:
            case TS_TYPE_MPEG2_AUDIO:
               pesFilterParams.pes_type = DMX_PES_AUDIO;
               break;
            default:
              pesFilterParams.pes_type = DMX_PES_OTHER;
              break;
          }
        }
        pesFilterParams.flags  = DMX_IMMEDIATE_START;

        if(ioctl(iFd, DMX_SET_PES_FILTER, &pesFilterParams) < 0)
        {
          Log(m_hLog, LOG_ERROR, C_String("Unable to set demux filter for PID ")
                + iPid + C_String("type : ") + pesFilterParams.pes_type);
          close(iFd);
        }
        else
        {
          LogDbg(m_hLog, C_String("Demux filter n°")+i+ C_String(" type: ") +
                  pesFilterParams.pes_type + C_String(" set for PID ") + iPid);
          m_iDemuxes[2 * i] = iPid;
          m_iDemuxes[2 * i + 1] = iFd;
        }
      }
    }
    else
    {
      LogDbg(m_hLog, C_String("Demux filter not started for PID ")
                    + iPid + C_String(" ES type: ") + iType);
    }
  }
  else
  {
     LogDbg(m_hLog, C_String("Too many Demux filter set"));	  
  }
}


//------------------------------------------------------------------------------
// Harware PID unselection
//------------------------------------------------------------------------------
void C_DvbInput::OnUnselectPid(u16 iPid)
{
  int i;
  for(i = 0; (i < 256) && (m_iDemuxes[2 * i] != iPid); i++);

  if(i < 256)
  {
    ASSERT(m_iDemuxes[2 * i] == iPid);
    ioctl(m_iDemuxes[2 * i + 1], DMX_STOP);
    close(m_iDemuxes[2 * i + 1]);
    m_iDemuxes[2 * i] = -1;
    LogDbg(m_hLog, C_String("Demux filter unset for PID ") + iPid);
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_DvbInput::HandleEvent(const C_Event& cEvent)
{
  ASSERT(false);
}


//------------------------------------------------------------------------------
// New version of the PAT
//------------------------------------------------------------------------------
void C_DvbInput::OnDvbPsiPatEvent(int iEvent)
{

  if(iEvent == DVBPSI_EVENT_CURRENT)
  {
    dvbpsi_pat_t *pLLPat = m_pCurrentPat->GetLowLevelPat();

    dvbpsi_pat_program_t* p_program = pLLPat->p_first_program;
    Log(m_hLog, LOG_NOTE, C_String("New PAT\n") );
    Log(m_hLog, LOG_NOTE, C_String("  transport_stream_id : ") + pLLPat->i_ts_id);
    Log(m_hLog, LOG_NOTE, C_String("  version_number      : ") + pLLPat->i_version);
    Log(m_hLog, LOG_NOTE, C_String("    | program_number @ [NIT|PMT]_PID"));
    while(p_program)
    {
      Log(m_hLog, LOG_NOTE, C_String("    | ") + p_program->i_number +
          C_String(" @ 0x") +p_program->i_pid + C_String(" (")
          + p_program->i_pid + C_String(")") );
      p_program = p_program->p_next;
    }
    Log(m_hLog, LOG_NOTE,   "  active              : " +pLLPat->b_current_next);

    C_DvbPsiPat DiffPatSub(0, 0, false);
    C_DvbPsiPat DiffPatAdd(0, 0, false);
    if(m_pPreviousPat)
    {
      DiffPatSub = *m_pPreviousPat - *m_pCurrentPat;
      DiffPatAdd = *m_pCurrentPat - *m_pPreviousPat;
    }
    else
    {
      DiffPatAdd = *m_pCurrentPat;
    }

    pLLPat = DiffPatSub.GetLowLevelPat();
    p_program = pLLPat->p_first_program;
    Log(m_hLog, LOG_NOTE,   "\n");
    Log(m_hLog, LOG_NOTE,   "Deleted programs\n");
    while(p_program)
    {
      Log(m_hLog, LOG_NOTE, C_String("    | ") + p_program->i_number +
          C_String(" @ 0x") +p_program->i_pid + C_String(" (") +
          p_program->i_pid + C_String(")") );
      p_program = p_program->p_next;
    }
    pLLPat = DiffPatAdd.GetLowLevelPat();
    p_program = pLLPat->p_first_program;
    Log(m_hLog, LOG_NOTE,   "\n");
    Log(m_hLog, LOG_NOTE,   "Added programs\n");
    while(p_program)
    {
      Log(m_hLog, LOG_NOTE, C_String("    | ") + p_program->i_number +
        C_String(" @ 0x") +p_program->i_pid + C_String(" (") +
        p_program->i_pid + C_String(")") );
      p_program = p_program->p_next;
    }

    m_cLock.Lock();
    m_cCurrentPat = *m_pCurrentPat;
    m_cLock.UnLock();

    // Kludge: signal the first PAT arrival.
    m_cEndInit.Protect();
    m_cEndInit.Signal();
    m_cEndInit.Release();
  }
}

//------------------------------------------------------------------------------
// Start the reception of the given program
//------------------------------------------------------------------------------
void C_DvbInput::OnStartStreaming(C_Broadcast* pBroadcast)
{

  // Get the channel Id
  int iIndex=m_vProgramNames.Find(pBroadcast->GetProgram()->GetName());
  LogDbg(m_hLog, "DVB Channel found: "+dvb->chans[iIndex].name);

  // Lock the demux usage
  m_cDemuxUsageM.Lock();

  //If we have not already started the demux, do so and wait for the first PAT
  if (m_iDemuxUsageCount == 0)
  {
    // Set the frontend up
    dvb->SetTP(dvb->chans[iIndex].tpid, dvb->chans[iIndex].satid);
    dvb->set_front();

    sleep(3);

    //Launch the demux
    m_pTrickPlay->Create();

    // Add a filter for PAT
    SelectPid(&m_cPatDecoder, 0x0000, TS_TYPE_NULL);

    // Wait for the first PAT
    m_cEndInit.Wait();
    m_cEndInit.Release();
    m_iGotTpid = dvb->chans[iIndex].tpid; // Remember the transponder
  }
  else
  {
    //Check that if we have already got one broadcast going this
    //new one is on the same mux (transponder)
    if(m_iGotTpid != dvb->chans[iIndex].tpid)
    {
      Log(m_hLog, LOG_ERROR, C_String("Attempting to start reception from" \
          " different transponder.Existing Transponder is") +m_iGotTpid +
          " New transponder is" + dvb->chans[iIndex].tpid);
      m_cDemuxUsageM.UnLock();
      return;
    }
  }

  // Update Demux Counter and unlock
  m_iDemuxUsageCount++;
  m_cDemuxUsageM.UnLock();

  // Get the program
  dvbpsi_pat_program_t *pProgram =
        m_cCurrentPat.GetProgram(dvb->chans[iIndex].pnr);

  m_cLock.Lock();

  // And launch the streaming line
  if(pProgram)
  {
    C_SyncFifo* pBuffer =
        new C_SyncFifo(2 * pBroadcast->GetChannel()->GetBuffCapacity());

    C_TsStreamer *pStreamer = new C_TsStreamer(m_hLog, pBroadcast,
                                               m_pTsProvider, pBuffer,
                                               m_pEventHandler, false, false);
    C_TsMux *pMux = new C_TsMux(m_pTsProvider, this, pBuffer);

    try
    {
      u16 iNumber = pBroadcast->GetProgram()->GetName().ToInt();

      pStreamer->Create();

      pMux->Attach();
      pMux->AttachProgram(pProgram->i_number, pProgram->i_pid);

      m_cMuxes.Add(iNumber, pMux);
      m_cStreamers.Add(iNumber, pStreamer);
    }
    catch(E_Exception e)
    {
      delete pStreamer;
      delete pMux;
      throw e;
    }
  }
  else
  {
    if (!m_bIgnoreMissing) {
      throw E_Exception(GEN_ERR, "Program \"" +
        pBroadcast->GetProgram()->GetName() +
        "\" doesn't exist");
    } else {
      Log(m_hLog, LOG_WARN, C_String("Ignoring missing program \"") +
      pBroadcast->GetProgram()->GetName() +
      C_String("\""));
    }
  }

  m_cLock.UnLock();
}


//------------------------------------------------------------------------------
// Resume the reception of the given program
//------------------------------------------------------------------------------
void C_DvbInput::OnResumeStreaming(C_Broadcast* pBroadcast)
{
}


//------------------------------------------------------------------------------
// Suspend the reception of the given program
//------------------------------------------------------------------------------
void C_DvbInput::OnSuspendStreaming(C_Broadcast* pBroadcast)
{
}

//------------------------------------------------------------------------------
// Forward the reception of the given program
//------------------------------------------------------------------------------
void C_DvbInput::OnForwardStreaming(C_Broadcast* pBroadcast, int speed)
{
}

//------------------------------------------------------------------------------
// Rewind the reception of the given program
//------------------------------------------------------------------------------
void C_DvbInput::OnRewindStreaming(C_Broadcast* pBroadcast, int speed)
{
}

//------------------------------------------------------------------------------
// Stop the reception of the given program
//------------------------------------------------------------------------------
void C_DvbInput::OnStopStreaming(C_Broadcast* pBroadcast)
{
  m_cLock.Lock();

  //Lock demux counter and decrement usage counter
  m_cDemuxUsageM.Lock();
  m_iDemuxUsageCount--;

  // If the usage counter is 0, we have to remove the PAT filter and suspend
  // the demux
  if(m_iDemuxUsageCount==0)
  {
    //Unset the filter on PAT
    UnselectPid(&m_cPatDecoder, 0x0000);
    m_pTrickPlay->Stop();
  }
  m_cDemuxUsageM.UnLock();

  u16 iNumber = pBroadcast->GetProgram()->GetName().ToInt();

  C_TsMux *pMux = m_cMuxes.Remove(iNumber);
  ASSERT(pMux);
  C_TsStreamer *pStreamer = m_cStreamers.Remove(iNumber);
  ASSERT(pStreamer);

  m_cLock.UnLock();

  pMux->Detach();
  delete pMux;

  try
  {
    pStreamer->Stop();
  }
  catch(E_Exception e)
  {
    delete pStreamer;
    throw E_Exception(GEN_ERR, "Unable to stop streaming of program "+
                      pBroadcast->GetProgram()->GetName(), e);
  }

  delete pStreamer;
}

//------------------------------------------------------------------------------
// Update configuration dynamically
//------------------------------------------------------------------------------
void C_DvbInput::OnUpdateProgram(C_String strProgram,
                                     C_String strFileName, C_String strType)
{
}

void C_DvbInput::OnDeleteProgram(C_String strProgram)
{
}
