/*******************************************************************************
* hashtable.cpp: Hashtable manipulation
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: hashtable.cpp,v 1.3 2003/08/05 23:18:19 nitrox Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Notice: This file must be included in the source file with its header
* TO DO:
* Warning: This class is not thread safe
*
*******************************************************************************/



//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
// There is no preamble since this file is to be included in the files which
// use the template: look at vector.h for further explanation







//******************************************************************************
// class C_HashMethod
//******************************************************************************
// This class has a specialisation for u16 and u32 to be able to use integers
// as key. Negative integers are not supported yet, they shouldn't be usefull
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> C_HashMethod<T>::C_HashMethod(u32 uiMaxHash)
{
  m_uiMaxHash = uiMaxHash;
}

C_HashMethod<u32>::C_HashMethod(u32 uiMaxHash)
{
  m_uiMaxHash = uiMaxHash;
}

C_HashMethod<u16>::C_HashMethod(u32 uiMaxHash)
{
  m_uiMaxHash = uiMaxHash;
}


C_HashMethod<handle>::C_HashMethod(u32 uiMaxHash)
{
  m_uiMaxHash = uiMaxHash;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> u32 C_HashMethod<T>::Hash(const T& cKey) const
{
  return cKey.GetHashCode() % m_uiMaxHash;
}

u32 C_HashMethod<u32>::Hash(u32 iKey) const
{
  return iKey % m_uiMaxHash;
}

u32 C_HashMethod<u16>::Hash(u16 iKey) const
{
  return iKey % m_uiMaxHash;
}

u32 C_HashMethod<handle>::Hash(handle hKey) const
{
  return (u32)hKey % m_uiMaxHash;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> u32 C_HashMethod<T>::GetMaxHash() const
{
  return m_uiMaxHash;
}

u32 C_HashMethod<u32>::GetMaxHash() const
{
  return m_uiMaxHash;
}

u32 C_HashMethod<u16>::GetMaxHash() const
{
  return m_uiMaxHash;
}

u32 C_HashMethod<handle>::GetMaxHash() const
{
  return m_uiMaxHash;
}



//******************************************************************************
// class C_Predicate
//******************************************************************************
// 
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> C_Predicate<T>::C_Predicate()
{
    // Nothing to do
}

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> bool C_Predicate<T>::Compare(const T& cArg1,
                                                const T& cArg2) const
{
  return cArg1 == cArg2;
}




//******************************************************************************
// class C_HashTableNode
//******************************************************************************
// 
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class Key, class Value> 
C_HashTableNode<Key, Value>::C_HashTableNode(const Key& cKey, Value* pValue) :
                                   m_cKey(cKey)
{
  ASSERT(pValue);
  
  m_pValue = pValue;
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class Key, class Value>
C_HashTableNode<Key, Value>::C_HashTableNode(const C_HashTableNode<Key, Value>& cNode) :
                                   m_cKey(cNode.m_cKey)
{
  // Copy the object stored in the node. Since the copy constructor cannot
  // be virtual, it wouldn't work to call new Value(*cNode.m_pValue) with
  // virtual classes: we therefore use the Clone() method
  ASSERT(m_pValue);
  m_pValue = cNode.m_pValue->Clone();
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class Key, class Value>
C_HashTableNode<Key, Value>::~C_HashTableNode()
{
  if(m_pValue)
    delete m_pValue;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class Key, class Value>
const Key& C_HashTableNode<Key, Value>::GetKey() const
{
  return m_cKey;
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class Key, class Value>
Value* C_HashTableNode<Key, Value>::GetValue() const
{
  return m_pValue;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class Key, class Value> void C_HashTableNode<Key, Value>::Empty()
{
  m_pValue = NULL;
}



//******************************************************************************
// class C_HashTable
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class Key, class Value>
C_HashTable<Key, Value>::C_HashTable(u32 uiBuckets) : m_cHashMethod(uiBuckets),
                                                      m_cPredicate()
{
  m_uiArraySize = m_cHashMethod.GetMaxHash();
  m_avData = new C_Vector< C_HashTableNode<Key, Value> >[m_uiArraySize];
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class Key, class Value>
C_HashTable<Key, Value>::C_HashTable(const C_HashTable<Key, Value>& cHashTable) :
   m_cHashMethod(cHashTable.m_cHashMethod), m_cPredicate(cHashTable.m_cPredicate)
{
  m_uiArraySize = m_cHashMethod.GetMaxHash();
  m_avData = new C_Vector< C_HashTableNode<Key, Value> >[m_uiArraySize];

  C_HashTableIterator<Key, Value> cIterator = cHashTable.CreateIterator();
  while(cIterator.HasNext())
  {
    C_HashTableNode<Key, Value>* pNode = cIterator.GetNext();
    Value* pValue = pNode->GetValue()->Clone();
    Add(pNode->GetKey(), pValue);
  }
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class Key, class Value> C_HashTable<Key, Value>::~C_HashTable()
{
  delete[] m_avData;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class Key, class Value>
C_HashTable<Key, Value>& C_HashTable<Key, Value>::operator =
                                 (const C_HashTable<Key, Value>& cHashTable)
{
  delete[] m_avData;

  m_uiArraySize = m_cHashMethod.GetMaxHash();
  m_avData = new C_Vector< C_HashTableNode<Key, Value> >[m_uiArraySize];

  C_HashTableIterator<Key, Value> cIterator = cHashTable.CreateIterator();
  while(cIterator.HasNext())
  {
    C_HashTableNode<Key, Value>* pNode = cIterator.GetNext();
    Value* pValue = pNode->GetValue()->Clone();
    Add(pNode->GetKey(), pValue);
  }

  return *this;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class Key, class Value>
void C_HashTable<Key, Value>::Add(const Key& cKey, Value* pValue)
{
  u32 uiHashCode = m_cHashMethod.Hash(cKey);
  C_Vector< C_HashTableNode<Key, Value> >& cVector = m_avData[uiHashCode];

#ifdef DEBUG  
  bool bFound = 0;
  unsigned int iIndex;
  for(iIndex = 0; iIndex < cVector.Size(); iIndex++)
  {
    bFound = m_cPredicate.Compare(cVector[iIndex].m_cKey, cKey);
    if(bFound)
      break;
  }
  ASSERT(!bFound);
#endif

  C_HashTableNode<Key, Value>* pNewNode = new C_HashTableNode<Key, Value> (cKey, pValue);
  cVector.Add(pNewNode);
}

  
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class Key, class Value>
Value* C_HashTable<Key, Value>::Remove(const Key& cKey)
{
  ASSERT(Get(cKey));

  u32 uiHashCode = m_cHashMethod.Hash(cKey);
  C_Vector< C_HashTableNode<Key, Value> >& cVector = m_avData[uiHashCode];
  Value* pItem = NULL;
  
  for(unsigned int iIndex = 0; iIndex < cVector.Size(); iIndex++)
  {
    bool bFound = m_cPredicate.Compare(cVector[iIndex].m_cKey, cKey);
    if(bFound)
    {
      C_HashTableNode<Key, Value>* pNode = cVector.Remove(iIndex);
      pItem = pNode->GetValue();
      pNode->Empty();
      delete pNode;
      break;
    }
  }

  ASSERT(pItem);
  return pItem;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class Key, class Value>
void C_HashTable<Key, Value>::Delete(const Key& cKey)
{
  ASSERT(Get(cKey));

  u32 uiHashCode = m_cHashMethod.Hash(cKey);
  C_Vector< C_HashTableNode<Key, Value> >& cVector = m_avData[uiHashCode];
  
  for(unsigned int iIndex = 0; iIndex < cVector.Size(); iIndex++)
  {
    bool bFound = m_cPredicate.Compare(cVector[iIndex].m_cKey, cKey);
    if(bFound)
    {
      cVector.Delete(iIndex);
      break;
    }
  }
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Update or create the value stored under Key, and destroy the old one if
// it existed
//------------------------------------------------------------------------------
template <class Key, class Value>
void C_HashTable<Key, Value>::Update(const Key& cKey, Value* pNewValue)
{
  Value* pOldValue = Modify(cKey, pNewValue);
  if(pOldValue)
    delete pOldValue;
}

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Return the value or NULL if it does not exist
//------------------------------------------------------------------------------
template <class Key, class Value>
Value* C_HashTable<Key, Value>::Get(const Key& cKey) const
{
  u32 uiHashCode = m_cHashMethod.Hash(cKey);
  C_Vector< C_HashTableNode<Key, Value> >& cVector = m_avData[uiHashCode];

  Value* pResult = NULL;
  unsigned int uiSize = cVector.Size();
  for(unsigned int iIndex = 0; iIndex < uiSize ; iIndex++)
  {
    bool bFound = m_cPredicate.Compare(cVector[iIndex].m_cKey, cKey);
    if(bFound)
    {
      pResult = cVector[iIndex].m_pValue;
      break;
    }
  }

  return pResult;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Return the old value or null if the entry does not exist, and set the entry
// to the new one. If the entry did not exist, it is added
//------------------------------------------------------------------------------
template <class Key, class Value>
Value* C_HashTable<Key, Value>::Modify(const Key& cKey, Value* pNewValue)
{
  u32 uiHashCode = m_cHashMethod.Hash(cKey);
  C_Vector< C_HashTableNode<Key, Value> >& cVector = m_avData[uiHashCode];

  Value* pResult = NULL;
  for(unsigned int iIndex = 0; iIndex < cVector.Size(); iIndex++)
  {
    bool bFound = m_cPredicate.Compare(cVector[iIndex].m_cKey, cKey);
    if(bFound)
    {
      pResult = cVector[iIndex].m_pValue;
      cVector[iIndex].m_pValue = pNewValue;
      break;
    }
  }
  
  if(pResult == NULL)
    Add(cKey, pNewValue);

  return pResult;  
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Returns the number of values stored in the hashtable
//------------------------------------------------------------------------------
template <class Key, class Value>
unsigned int C_HashTable<Key, Value>::Size() const
{
  unsigned int uiResult = 0;
  for(unsigned int i = 0; i < m_cHashMethod.GetMaxHash(); i++)
  {
    uiResult += m_avData[i].Size();
  }
  
  return uiResult;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Creates an iterator to browse the values contained in the hashtable.
//------------------------------------------------------------------------------
template <class Key, class Value>
C_HashTableIterator<Key, Value> C_HashTable<Key, Value>::CreateIterator() const
{
  return C_HashTableIterator<Key, Value>(*this);
}


//******************************************************************************
// class C_HashTableIterator
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Key, class Value>
C_HashTableIterator<Key, Value>::C_HashTableIterator(const C_HashTable<Key, Value>& cHashTable) : m_cHashTable(cHashTable)
{
  Reset();
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Key, class Value>
bool C_HashTableIterator<Key, Value>::HasNext()
{
  bool bResult = false;

  while(m_iCurrentVector < m_cHashTable.m_uiArraySize)
  {
    if(m_iCurrentVectorItem < m_cHashTable.m_avData[m_iCurrentVector].Size())
    {
      // We found an item
      bResult = true;
      break;
    }
    else
    {
      // Go to the beginning of the next vector in the buckets array 
      m_iCurrentVector++;
      m_iCurrentVectorItem = 0;
    }
  }

  return bResult;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Key, class Value>
C_HashTableNode<Key, Value>* C_HashTableIterator<Key, Value>::GetNext()
{
  ASSERT(HasNext());

  // Read the value at the current position
  C_Vector< C_HashTableNode<Key, Value> >& cVector = m_cHashTable.m_avData[m_iCurrentVector];
  C_HashTableNode<Key, Value>& cResult = cVector[m_iCurrentVectorItem];

  // Prepare the next iteration
  m_iCurrentVectorItem++;

  return &cResult;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class Key, class Value> void C_HashTableIterator<Key, Value>::Reset()
{
  m_iCurrentVector = 0;
  m_iCurrentVectorItem = 0; 
}

