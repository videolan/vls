/*******************************************************************************
* pipe.cpp: 
*-------------------------------------------------------------------------------
* (c)1999-2003 VideoLAN
* $Id$
*
* Authors: Tristan Leteurtre <tristan.leteurtre@anevia.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Notice: This file must be included in the source file with its header
* TO DO:
* Warning: This class is not thread safe
*
*******************************************************************************/


#include <unistd.h>
#include "pipe.h"


//******************************************************************************
// class C_DoublePipe
//******************************************************************************
// Use to communicate with an external program using its StdIn and StdErr
//           VLS Process
//    stdin1 ||      /\ stderr0          0 = read
//           ||      ||                  1 = write
//    stdin0 \/      || stderr1
//          Child Process
//******************************************************************************

C_DoublePipe::C_DoublePipe()
{
   pipe(m_iStdIn);
   pipe(m_iStdErr); 
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

int C_DoublePipe:: ChildDup2()
{
  int iRc = 0;
  iRc |= close(m_iStdIn[1]);
  iRc |= close(m_iStdErr[0]);
  iRc |= dup2(m_iStdIn[0], STDIN_FILENO);
  iRc |= dup2(m_iStdErr[1], STDERR_FILENO);
  iRc |= close(m_iStdIn[0]);
  iRc |= close(m_iStdErr[1]);

  return iRc;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

int C_DoublePipe::FatherClose()
{
  int iRc = 0;
  
  iRc |= close(m_iStdIn[0]);
  iRc |= close(m_iStdErr[1]);

  return iRc;
}
