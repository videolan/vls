/*******************************************************************************
* stach.h: Stack classes definitions
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: stack.h,v 1.3 2003/03/05 15:55:44 jpsaman Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _STACK_H_
#define _STACK_H_


//------------------------------------------------------------------------------
// class C_Fifo
//------------------------------------------------------------------------------
// Implementation of a FIFO stack
//------------------------------------------------------------------------------
template <class T> class C_Fifo
{
 public:
  C_Fifo(unsigned int iCapacity, byte bAutoClean = NO, bool bOverride = false);
  ~C_Fifo();
  C_Fifo(const C_Fifo<T>& cSrc);

  int Push(T* pData);
  T* Pop();
  T* Peek(unsigned int uiIndex);

  inline unsigned int Size() const
  { return   (m_iCapacity + 1 + (m_iWhereToPush - m_iWhereToPop))
	   % (m_iCapacity + 1); };
  inline unsigned int Capacity() const
  { return m_iCapacity; };

  T& operator [] (unsigned int iIndex) const;

  void Empty();
  
 protected:
  // Capacity of the buffer
  unsigned int m_iCapacity;
  // Where to push packets in the buffer ?
  unsigned int m_iWhereToPush;
  // Where to pull them ?
  unsigned int m_iWhereToPop;
  // Buffer
  T** m_apBuff;
  // Fifo behaviour options
  bool m_bOverride;
  // Memory management options
  byte m_bAutoClean;
};



#else
#error "Multiple inclusions of stack.h"
#endif

