/*******************************************************************************
* serialization.h: Serialization classes definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* The classes of this file provide an interface and some mechanisms that any
* class can use to become serializable
*
*******************************************************************************/


#ifndef _SERIALIZATION_H_
#define _SERIALIZATION_H_




//------------------------------------------------------------------------------
// C_Serializable class
//------------------------------------------------------------------------------
// This class is an interface that serializable classes must implement to
// become serializable.
//------------------------------------------------------------------------------
class C_Serializable
{
 public:
  virtual C_ClassDescription Reflect() const = 0;
  virtual ~C_Serializable(void) { }
};



//------------------------------------------------------------------------------
// C_ByteChains class
//------------------------------------------------------------------------------
// A chains of bytes arrays.
// In each array is stored the data for one type
//------------------------------------------------------------------------------
/*class C_ByteChains
{
 public:
  C_ByteChains();
  ~C_ByteChains();

  // ByteChains formatting
  u32 NextSize();
  byte* NextBytes();

  // ByteChains parsing
  void Append(byte* pData, u32 iSize);

 protected:
  u32 Size() const
  { return m_iSize; }
  const byte* Bytes() const
  { return m_pBuf; }

 private:
  byte* m_pBuf;
  u32 m_iSize;

  C_ByteChains* m_pCurrent;
};*/



//------------------------------------------------------------------------------
// C_Serializer class
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_Serializer
{
 public:
  // Constructor
  C_Serializer(const byte* pBaseAddress, const C_ClassDescription& cDescription);

  // Number of bytes to read/write at the next operation
  u32 NextBytesCount() const;
  // Reading of those bytes
  const byte* GetNextBytes();
  // Writing of those bytes
  void SetNextBytes(const byte* pBytes);

 protected:
  // Methods to help serialization of native types
/*  C_ByteChains Serialize(u32) const;
  C_ByteChains Serialize(u16) const;
  C_ByteChains Serialize(u8) const;

  C_ByteChains Serialize(s32) const;
  C_ByteChains Serialize(s16) const;
  C_ByteChains Serialize(s8) const;

  // Methods to help deserialization of native types
  u32 DeSerializeU32(const C_ByteChains& cData) const;
  u16 DeSerializeU16(const C_ByteChains& cData) const;
  u8 DeSerializeU8(const C_ByteChains& cData) const;

  s32 DeSerializeS32(const C_ByteChains& cData) const;
  s16 DeSerializeS16(const C_ByteChains& cData) const;
  s8 DeSerializeS8(const C_ByteChains& cData) const;
*/
 private:
  const byte* m_pBaseAddress;
  const C_ClassDescription m_cDescription;

  unsigned int m_iCurrentField;
};



#else
#error "Multiple inclusions of serialization.h"
#endif

