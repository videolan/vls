/*******************************************************************************
* library.h: Library Loader class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: library.h,v 1.2 2001/11/21 18:51:02 bozo Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Note: since the compiler also need the generic implementation code of the
* template to build the type specific byte code, the .cpp file must have to be
* also included in the source file
*
*******************************************************************************/


#ifndef _LIBRARY_H_
#define _LIBRARY_H_


//******************************************************************************
// Macro used to generate the entry points in the library
//******************************************************************************
// The first entry point will be used by the C_Library to create the instance of
// the class the C_Library is in charge of creating
// The second one will be used to init the library copies of the application
// global variables with the value of those variables in the application
//******************************************************************************
#define GENERATE_LIB_NOARG(class)                                              \
extern "C" {                                                                   \
  class* CreateInstance()                                                      \
  { return new class(); }                                                      \
                                                                               \
  int InitLib(C_Application* pApp)                                             \
  { C_Application::s_pApplication = pApp; return NO_ERR; }                     \
}

#define GENERATE_LIB_ARGS(class, args)                                         \
extern "C" {                                                                   \
  class* CreateInstance(args val)                                              \
  { return new class(val); }                                                   \
                                                                               \
  int InitLib(C_Application* pApp)                                             \
  { C_Application::SetApp(pApp); return NO_ERR; }                     \
}



//******************************************************************************
// class C_Library
//******************************************************************************
// It is a kind of class loader: it loads on the fly from a dynamic library
// the implementation of a class. The application therefore just needs to know
// the definition of this class or of any of its parents. 
//******************************************************************************
template <class Type, class Arg> class C_Library
{
 public:
  C_Library(const C_String& strName);
  ~C_Library();

  // Load / Unload the library
  void Load();
  void Unload();

  // Create an instance of the interface object of the library
  Type* CreateInstance(const Arg& cArg);

 protected:
  C_String GetLoaderError();
  
 private:
  // Library name
  C_String m_strName;
  // Handle to the open library
  handle m_hHandle;
  // Entry point in the library that will create the base object
  Type* (*m_InstanceCreator) (Arg);
};




#else
#error "Multiple inclusions of library.h"
#endif

