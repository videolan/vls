/*******************************************************************************
* iso-lang.cpp: ISO Languages tools
*-------------------------------------------------------------------------------
* (c)1999-2002 VideoLAN
* $Id: iso_lang.cpp,v 1.1 2002/05/14 22:10:08 bozo Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/

#include <string.h>
#include "iso_lang.h"

static iso639_lang_t p_iso_languages[] =
{
#define DEFINE_LANGUAGE_CODE(engName, nativeName, iso1, iso2T, iso2B) \
  { engName, nativeName, #iso1, #iso2T, #iso2B },
#include "iso-639.def"
};


iso639_lang_t * GetLang_1(const char * psz_iso639_1)
{
  unsigned int i;
  for(i = 0; i < sizeof(p_iso_languages)/sizeof(iso639_lang_t); i++)
  {
    if(!strncmp(p_iso_languages[i].psz_iso639_1, psz_iso639_1, 2))
      break;
  }
  if(i < sizeof(p_iso_languages)/sizeof(iso639_lang_t))
    return &p_iso_languages[i];
  else
    return NULL;
}

iso639_lang_t * GetLang_2T(const char * psz_iso639_2T)
{
  unsigned int i;
  for(i = 0; i < sizeof(p_iso_languages)/sizeof(iso639_lang_t); i++)
  {
    if(!strncmp(p_iso_languages[i].psz_iso639_2T, psz_iso639_2T, 2))
      break;
  }
  if(i < sizeof(p_iso_languages)/sizeof(iso639_lang_t))
    return &p_iso_languages[i];
  else
    return NULL;
}

iso639_lang_t * GetLang_2B(const char * psz_iso639_2B)
{
  unsigned int i;
  for(i = 0; i < sizeof(p_iso_languages)/sizeof(iso639_lang_t); i++)
  {
    if(!strncmp(p_iso_languages[i].psz_iso639_2B, psz_iso639_2B, 2))
      break;
  }
  if(i < sizeof(p_iso_languages)/sizeof(iso639_lang_t))
    return &p_iso_languages[i];
  else
    return NULL;
}

