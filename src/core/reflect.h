/*******************************************************************************
* reflect.h: Reflective information classes definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: reflect.h,v 1.1 2001/10/06 21:23:36 bozo Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* This file provide classes to build and/or obtain reflective information
* about classes
*******************************************************************************/


#ifndef _REFLECT_H_
#define _REFLECT_H_



//------------------------------------------------------------------------------
// Defines
//------------------------------------------------------------------------------
// Not applicable
#define UNKNOWN   0

// Field type
#define SCALAR    1
#define ARRAY     2
#define STRUCT    3
#define CLASS     4

// Field base type
#define BYTE      11
#define CHAR      12
#define U8        21
#define U16       22
#define U32       23
#define S8        31
#define S16       32
#define S32       33




//------------------------------------------------------------------------------
// Forward declaration
//------------------------------------------------------------------------------
class C_ClassDescription;


//------------------------------------------------------------------------------
// C_ClassField class
//------------------------------------------------------------------------------
// To split in 3 classes
//------------------------------------------------------------------------------
class C_ClassField
{
  friend class C_ClassDescription;

 public:
  // For scalar types
  C_ClassField(byte iScalarType, u32 iOffset);
  // For arrays
  C_ClassField(byte iScalarType, u32 iSize, u32 iOffset);
  // For subclasses
  C_ClassField(u32 iSubTypesCount);

  // Return the field type
  byte GetFieldType() const
  { return m_bFieldType; }
  // Return the scalar type if applicable (ie if it's a scalar or an array)
  byte GetScalarType() const
  { return m_bScalarType; }
  // Return the offset of the field in the structure
  u32 GetOffset() const
  { return m_iOffset; }
  // Return the number of subtypes of the type
  u32 GetSubTypesCount() const
  { return m_iSubTypes; }

 protected:
  C_ClassField();

 private:
  byte m_bFieldType;
  byte m_bScalarType;
  u32 m_iOffset;
  u32 m_iSubTypes;

  C_ClassField* m_aSubTypes;
};



//------------------------------------------------------------------------------
// C_ClassMethod class
//------------------------------------------------------------------------------
// To write
//------------------------------------------------------------------------------
class C_ClassMethod
{
  friend class C_ClassDescription;

 public:
  C_ClassMethod(byte bReturnType, byte aArgTypes[]);

 protected:
  C_ClassMethod();
};



//------------------------------------------------------------------------------
// C_ClassDescription class
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_ClassDescription
{
 public:
  C_ClassDescription(unsigned int iFields, unsigned int iMethods);
  ~C_ClassDescription();

  void AddField(const C_ClassField& cField);
  void AddMethod(const C_ClassMethod& cMethod);

  unsigned int GetFieldsCount() const
  { return m_iFields; }
  unsigned int GetMethodsCount() const
  { return m_iMethods; }

  const C_ClassField& GetField(unsigned int iField) const;
  const C_ClassMethod& GetMethod(unsigned int iMethod) const;

 private:
  C_ClassField* m_aFields;
  C_ClassMethod* m_aMethods;

  unsigned int m_iFields;
  unsigned int m_iMethods;

  unsigned int m_iCurrentField;
  unsigned int m_iCurrentMethod;
};



#else
#error "Multiple inclusions of reflect.h"
#endif

