/*******************************************************************************
* interface.h:
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: interface.h,v 1.6 2003/08/05 19:55:15 tooney Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _INTERFACE_H_
#define _INTERFACE_H_


//------------------------------------------------------------------------------
// Native admin module definitions
//------------------------------------------------------------------------------
#define INIT_PHASE              1
#define LOGIN_PHASE             2
#define COMMAND_PHASE           4


//------------------------------------------------------------------------------
// E_Interface class
//------------------------------------------------------------------------------
class E_Interface : public E_Exception
{
 public:
  E_Interface(const C_String& strMsg);
  E_Interface(const C_String& strMsg, const E_Exception& e);
};

//------------------------------------------------------------------------------
// E_Interface class
//------------------------------------------------------------------------------
class C_InterfaceConfig
{
public:
  handle                hLog;
  C_Admin*              pAdmin;
};

//------------------------------------------------------------------------------
// C_InterfaceSession class
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_InterfaceSession : public C_AdminSession
{
public:
  C_InterfaceSession(C_Socket* pConnection, void* pAdmin);
  ~C_InterfaceSession();

  // Event management
  virtual void SendEvent(const C_Event& cEvent);

  C_String m_strPeerName;

protected:
  virtual void OnInit();
  virtual void OnClose();

  // Connection to the peer
  C_Socket* m_pConnection;
  C_Stream<C_Socket> m_cStream;

  // Session status
  int m_iPhase;

};



//------------------------------------------------------------------------------
// C_Interface class
//------------------------------------------------------------------------------
class C_Interface : public C_Thread
{
public:
  C_Interface(C_Module* pModule, C_InterfaceConfig& cConfig);
  ~C_Interface();

  virtual void PropagateEvent(const C_Event& cEvent);

  virtual int Init() = 0;
  int Run();
  int StopInterface();
  int Destroy();

  void SetConfig(C_String sModuleName);

  C_String GetName() {return m_sInterfaceName;};


protected:

  virtual void InitSession() = 0;
  virtual void StopSession() = 0;
  virtual void RunSession() = 0;
  virtual void DestroySession() = 0;

  /* Thread virtual methods implementation */
  virtual void InitWork();
  virtual void DoWork();
  virtual void StopWork();
  virtual void CleanWork();

  // Helpers
  handle m_hLog;
  C_Admin* m_pAdmin;

  // Event fifo
  C_Vector<C_Event> m_cEvents;
  C_Mutex m_cEventsLock;

  C_String m_sInterfaceName;
  C_Module* m_pModule;
};


DECLARE_VIRTUAL_MODULE(Interface, "interface",C_InterfaceConfig&);

#else
#error "Multiple inclusions of interface.h"
#endif

