/*******************************************************************************
* broadcast.h: Broadcast class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: broadcast.h,v 1.4 2003/07/31 14:46:37 nitrox Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _BROADCAST_H_
#define _BROADCAST_H_


//------------------------------------------------------------------------------
// Forward declaration
//------------------------------------------------------------------------------
class C_Input;
class C_Channel;
class C_Program;


//------------------------------------------------------------------------------
// Defines
//------------------------------------------------------------------------------
#define BROADCAST_WAITING       1
#define BROADCAST_RUNNING       2
#define BROADCAST_STOPPED       3
#define BROADCAST_SUSPENDED     4             
#define BROADCAST_ERROR         5
#define BROADCAST_FORWARD       6
#define BROADCAST_REWIND        7

//------------------------------------------------------------------------------
// C_Broadcast
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class C_Broadcast
{
 public:
  // Class construction
  C_Broadcast(const C_Program* pProgram, C_Input* pInput, C_Channel* pChannel,
                                                C_String sName);
  void SetStatus(byte bStatus)
  { m_bStatus = bStatus; }

  // Accessors
//  const C_String& GetPgrmName() const
//  { return m_pProgram->GetName(); }

  const C_Program* GetProgram() const
  { return m_pProgram; }
  C_Input* GetInput() const
  { return m_pInput; }
  C_Channel* GetChannel() const
  { return m_pChannel; }

  const C_String GetName() const
  { return m_strName; }

  byte GetStatus() const
  { return m_bStatus; }

  void SetOption(const C_String& strName, const C_String strValue);
  const C_String GetOption(const C_String& strName) const;
  void SetOptions(const C_HashTable<C_String, C_String>& cOptions);

 private:
  C_String m_strName;
  const C_Program* m_pProgram;
  C_Input* m_pInput;
  C_Channel* m_pChannel;

  C_HashTable<C_String, C_String> m_cOptions;

  byte m_bStatus;
};



#else
#error "Multiple inclusions of broadcast.h"
#endif

