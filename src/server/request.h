/*******************************************************************************
* request.h: User requests encapsulation
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/

#ifndef _REQUEST_H_
#define _REQUEST_H_


class C_Message;
class C_Broadcast;

/* Error codes for answer */
#define ANS_NO_ERR            0x200             //OK
#define ANS_AUTH_SUCCESS      0x200             //OK
#define ANS_ALREADY_STREAMING 0x201             // Broadcast is already
                                                // streaming
#define ANS_AUTH_FAILED       0x401             // Authentification failed
#define ANS_CMD_FORBIDDEN     0x403             // Command not allowed
#define ANS_PROGRAM_UNKNOWN   0x404             // Program is unknown
#define ANS_CMD_NOTLOGIN      0x455             // Not authenticated

#define ANS_INTERNAL_ERROR    0x500             // Internal error

#define ANS_SYNTAX_ERROR      0x601             // Malformed request
#define ANS_CHANNEL_UNKNOWN   0x611             // Channel is unknown
#define ANS_CHANNEL_BUSY      0x612             // Channel is busy

#define ANS_INPUT_UNKNOWN     0X404             // Input is unknown

#define ANS_BROADCAST_UNKNOWN 0x641             // Broadcast is unknown
#define ANS_BROADCAST_BUSY    0x642             // Broadcast is busy

//------------------------------------------------------------------------------
// C_Request
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_Request
{
  friend class C_Message;

public:
  C_Request(const C_String& strCmd);
  
  // Push an argument and its value
  void SetArg(const C_String strArgName, const C_String& strArgValue);

  // Cmd name
  const C_String& GetCmd() const
  { return m_strCmd; }
  // Other arguments in the request (if any)
  C_String GetArg(const C_String& strArgName) const;
  const C_HashTable<C_String, C_String>& GetArgs() const;

private:
  C_String m_strCmd;
  C_HashTable<C_String, C_String> m_cArgs;
};



//------------------------------------------------------------------------------
// C_Answer
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_Answer
{
  friend class C_Message;

public:
  C_Answer(const C_String& strProvider);

  // Get the module that produced this answer
  const C_String& GetProvider() const;
  // Get the status code (see common.h for code definition)
  s32 GetStatus() const;
  // Return a list of message describing the error
  const C_List<C_String>& GetMessages() const;
  // Return a list of chained answers
  const C_List<C_Answer>& GetSubAnswers() const;

  // Method used to store the previous information in the class
  void SetStatus(s32 iStatus);
  void AddMessage(const C_String& strMessage);
  void Add(const C_Answer& cAnswer);

  C_Answer* Clone();

private:
  // Answer source
  C_String m_strProvider;

  // Request status
  s32 m_iStatus;
  C_List<C_String> m_cMessages;

  // List of subanswers (if any)
  C_List<C_Answer> m_cAnswerList;
};


//------------------------------------------------------------------------------
// C_Event
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------

class C_Event
{
  friend class C_Message;

public:
  C_Event(const C_String& strProvider);

  // Get the module that produced this event
  const C_String& GetProvider() const;
  // Get the event type
  s32 GetCode() const;
  // Get the broadcast info related to the event
  const C_Broadcast* GetBroadcast() const;

  // Return a list of message
//  C_String GetProperty(const C_String& strPropName) const;
  const C_List<C_String>& GetMessages() const;

  // Method used to store the previous information in the class
  void SetCode(s32 iCode);
  void SetBroadcast(const C_Broadcast* pBroadcast);
//  void SetProperty(const C_String strPropName, const C_String& strPropValue);
  void AddMessage(const C_String& strMessage);

  C_Event* Clone();

private:
  // Event source
  C_String m_strProvider;
 
  // Event type
  s32 m_iCode;

  // Related broadcast
  const C_Broadcast* m_pBroadcast;

//  C_HashTable<C_String, C_String> m_cProperties;
  C_List<C_String> m_cMessages;
};


//------------------------------------------------------------------------------
// C_Message : container for a 'C_Request' or a 'C_Answer' or a 'C_Event'.
//------------------------------------------------------------------------------
// It will be used to stream the objects contained on an admin connection for
// example.
//------------------------------------------------------------------------------

#define INVALID_TYPE 0
#define REQUEST_TYPE 1
#define ANSWER_TYPE  2
#define EVENT_TYPE   3

#define INVALID_TYPE_STR "0"
#define REQUEST_TYPE_STR "1"
#define ANSWER_TYPE_STR  "2"
#define EVENT_TYPE_STR   "3"

class C_Message
{
public:
  C_Message(const C_Request& cRequest);
  C_Message(const C_Answer& cAnswer);
  C_Message(const C_Event& cEvent);

  C_Message(const C_String& strMessage); // reverse streaming (temporary)
  C_String GetString(); // streaming (temporary)

  s32 GetType() const { return m_iType; };
  C_Request GetRequest() const
  {
    ASSERT(m_iType == REQUEST_TYPE);
    return m_cRequest;
  };
  C_Answer GetAnswer() const
  {
    ASSERT(m_iType == ANSWER_TYPE);
    return m_cAnswer;
  };
  C_Event GetEvent() const
  {
    ASSERT(m_iType == EVENT_TYPE);
    return m_cEvent;
  };

private:
  C_Answer* ExtractAnswer(C_StringTokenizer &cTokenizer);
  C_String AnswerToString(C_Answer &cAnswer);

  s32 m_iType;  // 0: invalid, 1: request, 2: answer, 3: event
  C_Request m_cRequest;
  C_Answer m_cAnswer;
  C_Event m_cEvent;
};


//------------------------------------------------------------------------------
// C_RequestHandler
//------------------------------------------------------------------------------
// All the classes that are able to handle any request should implement this
// interface to avoid dependancies between modules
//------------------------------------------------------------------------------
class C_RequestHandler
{
public:
  virtual C_Answer HandleRequest(C_Request& cRequest) = 0;
  virtual ~C_RequestHandler(void) { }
};


//------------------------------------------------------------------------------
// C_EventHandler
//------------------------------------------------------------------------------
// All the classes that are able to handle any event should implement this
// interface to avoid dependancies between modules
//------------------------------------------------------------------------------
class C_EventHandler// : public C_Thread
{
public:
  virtual void HandleEvent(const C_Event& cEvent) = 0;
  virtual ~C_EventHandler(void) { }
};


//------------------------------------------------------------------------------
// C_RequestHub
//------------------------------------------------------------------------------
// All the classes that have to propagate any request should implement this
// interface to avoid dependancies between modules
//------------------------------------------------------------------------------
class C_RequestHub
{
public:
  virtual C_Answer ForwardRequest(C_Request& cRequest) = 0;
  virtual ~C_RequestHub(void) { }
};


//------------------------------------------------------------------------------
// C_EventHub
//------------------------------------------------------------------------------
// All the classes that have to propagate any event should implement this
// interface to avoid dependancies between modules
//------------------------------------------------------------------------------
class C_EventHub
{
public:
  virtual void ForwardEvent(const C_Event& cEvent) = 0;
  virtual ~C_EventHub(void) { }
};



#else
#error "Multiple inclusions of request.h"
#endif

