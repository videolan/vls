/*******************************************************************************
* vls.h: vls class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: vls.h,v 1.5 2003/08/13 15:59:31 nitrox Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _VLS_H_
#define _VLS_H_

//------------------------------------------------------------------------------
// C_Vls class
//------------------------------------------------------------------------------
class C_Vls :  public C_Application,
#ifndef  _WIN32
              public C_Daemon,
#endif
              public C_RequestHub, public C_EventHub
{
public:
  C_Vls(const C_String& strName);
  ~C_Vls();

  int SafeStop();
  
protected:
  virtual int OnAppInit();
  virtual int OnAppRun();
  virtual int OnAppExit();
  virtual int OnAppDestroy();

  virtual C_Answer ForwardRequest(C_Request& cRequest);
  virtual void ForwardEvent(const C_Event& cEvent);

  int CheckEnvir();
  void Usage(const C_String& strProgName);
  int GetConfig(int iArgc, char** paArg, C_String* pstrCfgFile, C_String* pstrLogFile);

private:
  void LoadBuiltins();

  C_Manager* m_pManager;
  C_Admin* m_pAdministrator;
};


#else
#error "Multiple inclusions of vls.h"
#endif

