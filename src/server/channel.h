/*******************************************************************************
* channel.h: Channel Encapsulation
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: channel.h,v 1.5 2003/11/18 17:09:40 tooney Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _CHANNEL_H_
#define _CHANNEL_H_

// Yes I know this is properly ugly
// But it avoids 8 lookup in the hashtable when adding a channel
// in real-time which can save precious cpu
// -- 
// nitrox
struct C_ChannelConfig
{
   C_String m_strName;
   C_String m_strSrcHost;
   C_String m_strSrcPort;
   C_String m_strDstHost;
   C_String m_strDstPort;
   C_String m_strType;
   C_String m_strStreamType;
   C_String m_strDomain;
   C_String m_strInterface;
   C_String m_strFilename;
   bool m_bAppend;
   int m_iTTL;
};


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_Channel
{
 public:
  C_Channel(C_Module* pModule, const C_ChannelConfig& cConfig);
  virtual ~C_Channel();
  
  // The corresponding output must be initialised before use and destroyed
  // after use
  C_Output* GetOutput();
  void ReleaseOutput();

  // channel info
  C_String GetName() const
  { return m_strName; }
  unsigned int GetBuffCapacity() const
  { return m_pOutput->GetBuffCapacity(); }

  // Is channel already used ?
  bool IsFree() const
  { return m_bIsFree; }

  void SetHelperProcessId(int Id) {iHelperProcessId=Id; };
  int  GetHelperProcessId() {return iHelperProcessId; };

 protected:
  C_String m_strName;
  C_Output* m_pOutput;
  bool m_bIsFree;
  C_Module* m_pModule;

  int iHelperProcessId;
};


DECLARE_VIRTUAL_MODULE(Channel, "channel", const C_ChannelConfig&);


#else
#error "Multiple inclusions of channel.h"
#endif

