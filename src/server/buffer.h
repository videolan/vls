/*******************************************************************************
* buffer.h: Buffer classes definitions
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: buffer.h,v 1.7 2003/08/14 17:20:36 adq Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _SERVER_BUFFER_H_
#define _SERVER_BUFFER_H_


//------------------------------------------------------------------------------
// class C_SyncFifo
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class C_SyncFifo : public I_TsPacketHandler
{
 public:
  C_SyncFifo(unsigned int uiSize);
  virtual ~C_SyncFifo();

  unsigned int Capacity() const
  { return m_cFifo.Capacity(); }
  unsigned int Size() const
  { return m_cFifo.Size(); }

  virtual bool HandlePacket(C_TsPacket* pPacket);
  virtual bool HandlePrefillPacket(C_TsPacket* pPacket);
  virtual void PrefillStart();
  virtual void PrefillComplete();
  virtual void Shutdown();
  C_TsPacket* Pop();
  C_TsPacket* Peek(unsigned int uiIndex);

 private:
  // Thread synchro objects
  C_Semaphore m_cNotEmptySignal;
  C_Semaphore m_cNotFullSignal;
  C_Condition m_cPrefilling;

  // Fifo
  C_Fifo<C_TsPacket> m_cFifo;

  bool m_bShutdown;
};

//------------------------------------------------------------------------------
// C_DatedBuffer class
//------------------------------------------------------------------------------
// Buffer class that allows for timestamping data.
//------------------------------------------------------------------------------
class C_DatedBuffer
{
public:
  C_DatedBuffer()
    { m_pData = NULL;
      m_iSize = 0;
      m_TimeStamp = 0; }
  ~C_DatedBuffer()
    { if( m_pData ) free( m_pData ); }
  bool Alloc( int iSize )
    { m_pData = (byte*)malloc( iSize );
      return ( m_pData != NULL ); }

  byte * m_pData;
  int m_iSize;
  u64 m_TimeStamp;
};

class C_DatedFifo
{
public:
  C_DatedFifo(unsigned int uiSize);
  virtual ~C_DatedFifo();

  unsigned int Capacity() const
  { return m_cFifo.Capacity(); }
  unsigned int Size() const
  { return m_cFifo.Size(); }

  C_DatedBuffer* Pop(void);
  int Push(C_DatedBuffer *bufDated);

private:
  C_Fifo<C_DatedBuffer> m_cFifo;
  C_Mutex m_cMutex;
};

//------------------------------------------------------------------------------
// Class C_NetList
//------------------------------------------------------------------------------
// The netlist doe not inherit from any buffer for several reasons:
// * this has been tried, and ununderstandable problems occurred
// * it must be possible to implement it as LIFO if it is proved that it's
// more efficient and there is no LIFO class
//------------------------------------------------------------------------------
class C_NetList
{
 public:
  C_NetList(unsigned int iSize = 65536);
  ~C_NetList();
  
  C_TsPacket* GetPacket();
  void RefPacket(C_TsPacket* pPacket);
  void ReleasePacket(C_TsPacket* pPacket);

  unsigned int Capacity()
  { return iBuffSize; }

 protected:
  // The buffer of TS packets itself
  C_TsPacket* aPackets;
  // Array of pointers to the free packets (ie that never were poped) and
  // index of the first element of the table that points to a such packet
  C_TsPacket** apFreePackets;
  unsigned int iFirstFree;
  // Size of the 2 previous arrays
  unsigned int iBuffSize;

 private:
  // Mutex used to allow the netlist to be used by several threads at the same
  // time
  C_Mutex m_sMutex;
};


#else
#error "Multiple inclusions of buffer.h"
#endif

