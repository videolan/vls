/*******************************************************************************
* converter.h: MPEG converters
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: converter.h,v 1.7 2003/08/08 16:54:18 tooney Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _MPEG_CONVERTER_H_
#define _MPEG_CONVERTER_H_



//------------------------------------------------------------------------------
// C_MpegConverterConfig class
//------------------------------------------------------------------------------
// build parameters of C_MpegConverter
//------------------------------------------------------------------------------
class C_MpegConverterConfig
{
public:
  handle                m_hLog;

  C_Broadcast*          m_pBroadcast;
  C_MpegReader*         m_pReader;
  C_NetList*            m_pTsProvider;
  C_EventHandler*       m_pEventHandler;
};


//------------------------------------------------------------------------------
// C_MpegConverter class
//------------------------------------------------------------------------------
// Base class for converters
//------------------------------------------------------------------------------
class C_MpegConverter
{
public:
  C_MpegConverter(C_Module* p_Module,
                  C_MpegConverterConfig& cConfig);
  virtual ~C_MpegConverter();

  virtual s64 GetNextTsPackets(C_Fifo<C_TsPacket> *, int skip = 0);
  virtual s64 GetPrevTsPackets(C_Fifo<C_TsPacket> *, int skip = 0);
  virtual void InitWork(); 
  virtual void CleanWork();
  virtual void StopWork();

protected:
  C_Module* m_pModule;

  handle m_hLog;

  C_Broadcast           *m_pBroadcast;
  C_MpegReader          *m_pReader;
  C_NetList             *m_pTsProvider;
  C_EventHandler*        m_pEventHandler;
};


// Declaration and implementation of C_MpegConverterModule which has to be
// inherited by each input module => C_????MpegConverterModule
DECLARE_VIRTUAL_MODULE(MpegConverter, "mpegconverter", C_MpegConverterConfig&);


#else
#error "Multiple inclusions of converter.h"
#endif

