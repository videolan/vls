/*******************************************************************************
* pes.cpp: PES facilities
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: pes.cpp,v 1.5 2003/10/27 10:58:11 sam Exp $
*
* Authors: Tristan Leteurtre <tooney@videolan.org>
*          Jean-Paul Saman <jpsaman@wxs.nl>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../core/core.h"
#include "mpeg.h"
#include "ts.h"
#include "pes.h"



//******************************************************************************
// C_PesPacket class
//******************************************************************************
//
//******************************************************************************

C_PesPacket::C_PesPacket()
{
  m_iRefCount = 0;
  m_pTsPacket = NULL;
  bData = new byte [PES_MAX_SIZE];
}

C_PesPacket::C_PesPacket(C_TsPacket *pPacket)
{
  ASSERT(pPacket);
  m_iRefCount = 0;
  m_pTsPacket = pPacket;
  bData = pPacket->GetPayloadStart();
}

C_PesPacket::~C_PesPacket()
{
  if (m_pTsPacket)
  {
    m_pTsPacket = NULL;
    bData = NULL;  
  }
  if (bData) delete bData;
}

void C_PesPacket::SetTsPacket(C_TsPacket *pPacket)
{
  if (m_pTsPacket)
  {
    ASSERT(m_pTsPacket);
    if (bData) delete bData;
    bData = NULL;
    m_pTsPacket = NULL;
  }
  ASSERT(pPacket);
  m_pTsPacket = pPacket;
  bData = pPacket->GetPayloadStart();
};

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Give direct access to the raw buffer
//------------------------------------------------------------------------------
C_PesPacket::operator byte* ()
{
  ASSERT(bData);
  return bData;
}

u8 C_PesPacket::BuildPacket(u64 Pts, u8 iPayloadType, byte * Payload, unsigned int iPayloadSize)
{
  if (!Pts) m_iHeaderSize = 9;
    else m_iHeaderSize = 14;

  m_iPesSize = m_iHeaderSize + iPayloadSize;

  // Pes start code
  bData[0] = 0x00;
  bData[1] = 0x00;
  bData[2] = 0x01;
  bData[3] = iPayloadType;
  bData[4] = (m_iPesSize - 6) >> 8;
  bData[5] = (m_iPesSize - 6) & 0xFF;
  bData[6] = 0x80;
  bData[7] = Pts ? 0x80 : 0; // PTS Flags or not
  bData[8] = Pts ? 5 : 0; // Number of following bytes of the header

  if (Pts)
  {
    bData[9] = 0x21 | ((Pts >> 29) & 0x0E);
    bData[10] =  (Pts >>22 & 0xFF);
    bData[11] = 0x01 | ((Pts >> 14 ) & 0xFE);
    bData[12] =  (Pts >> 7 & 0xFF);
    bData[13] = 0x01 | ((Pts << 1 ) & 0xFE);
  }

  memcpy(bData + m_iHeaderSize, Payload, iPayloadSize);

  return 0;
}

pes_frame_t C_PesPacket::GetFrameType()
{
  ASSERT(bData);

  u8 payloadLength = m_pTsPacket->GetPayloadLength();

  // Find picture start code.
  for (int i=0; i<(payloadLength-5); i++)
  {
    if ( (bData[i]   == (byte) 0x00) &&
         (bData[i+1] == (byte) 0x00) &&
         (bData[i+2] == (byte) 0x01) &&
         (bData[i+3] == (byte) 0x00) )
    {
      // Picture start code found.
      u8 picture_coding_type = ((bData[i+5]&0x38)>>3);
      switch (picture_coding_type)
      {
        case 0x00:
          return FORBIDDEN;
        case 0x01:
          return I_FRAME;
        case 0x02:
          return P_FRAME;
        case 0x03:
          return B_FRAME;
        case 0x04:
          return NOT_USED;
        case 0x05:
          return RESERVED_1;
        case 0x06:
          return RESERVED_2;
        case 0x07:
          return RESERVED_3;
      }
    }
  }
  return NONE;
}

