/*******************************************************************************
* streamdescr.cpp: MPEG stream description
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: streamdescr.cpp,v 1.6 2003/10/27 10:58:11 sam Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* TODO: DTS support
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#if defined(HAVE_INTTYPES_H)
#   include <inttypes.h>
#elif defined(HAVE_STDINT_H)
#   include <stdint.h>
#endif

#ifdef HAVE_DVBPSI_DVBPSI_H
#   include <dvbpsi/dvbpsi.h>
#   include <dvbpsi/descriptor.h>
#   include <dvbpsi/pat.h>
#   include <dvbpsi/pmt.h>
#   include <dvbpsi/dr.h>
#else
#   include "src/dvbpsi.h"
#   include "src/descriptor.h"
#   include "src/tables/pat.h"
#   include "src/tables/pmt.h"
#   include "src/descriptors/dr.h"
#endif

#include "../core/core.h"
#include "mpeg.h"
#include "ts.h"
#include "streamdescr.h"


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_ElementDescriptor::AddLanguageCode(const char *pszCode)
{
  dvbpsi_iso639_dr_t sDecoded;
  sDecoded.i_code_count = 1;
  memcpy(sDecoded.i_iso_639_code, pszCode, 3);
  sDecoded.i_audio_type = 0x01;

  dvbpsi_descriptor_t *pDescriptor = dvbpsi_GenISO639Dr(&sDecoded, 0);

  dvbpsi_descriptor_t *pDr = m_pDvbPsiEs->p_first_descriptor;
  if(!pDr)
  {
    m_pDvbPsiEs->p_first_descriptor = pDescriptor;
  }
  else
  {
    while(pDr->p_next != NULL)
      pDr = pDr->p_next;
    pDr->p_next = pDescriptor;
  }
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_ProgramDescriptor::C_ProgramDescriptor(unsigned int iMpegVersion)
{
  ASSERT(iMpegVersion == 1 || iMpegVersion == 2);

  memset(m_apElementaryStreams, 0, sizeof(m_apElementaryStreams));  
  memset(m_apP1ElementaryStreams, 0, sizeof(m_apP1ElementaryStreams));  

  // A virer
  ASSERT(sizeof(m_apElementaryStreams) == 4*0xFF);

  // Init the PAT and the PMT. The PMT for the program will be carried on the
  // 0x50 pid as we're sure that there is nothing else here. The PCR_PID is set
  // to 0x0 since it is legal and we don't have any other at that time
  dvbpsi_InitPAT(&m_sPat, 1, 0, 1);
  dvbpsi_PATAddProgram(&m_sPat, 0x10, 0x50);

  dvbpsi_InitPMT(&m_sPmt, 0x10, 0, 1, 0);

  m_bNewPmt = true;

  m_iNextPid = 0x80;
  m_iMpegVersion = iMpegVersion;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_ElementDescriptor* C_ProgramDescriptor::AddES(u8 iId, u8 iSubId)
{
  C_ElementDescriptor* pDescr;
  if(iId == PES_ID_PRIVATE_1)
    pDescr = m_apP1ElementaryStreams[iSubId];
  else
    pDescr = m_apElementaryStreams[iId];

  if(pDescr)
  {
    // pgrm already added -> just add the descriptors if any (to do)
  }
  else
  {
    u16 iPid = GetNewPid();
    printf("New Pid assigned: %x\n", iPid);
    dvbpsi_pmt_es_t *pDvbPsiEs = dvbpsi_PMTAddES(&m_sPmt, 0, iPid);
    pDescr = new C_ElementDescriptor(iId, pDvbPsiEs);
    if(iId == PES_ID_PRIVATE_1)
      m_apP1ElementaryStreams[iSubId] = pDescr;
    else
      m_apElementaryStreams[iId] = pDescr;

    // Update the PMT. The data we filter are not the correct ones -> to 
    // change
    // MPEG video ES
    if(iId >= PES_ID_MIN_VIDEO && iId <= PES_ID_MAX_VIDEO)
    {
      printf("Video: 0x%x , %d\n", pDescr->GetPid(), pDescr->GetPid());
      if(m_iMpegVersion == 1)
        pDvbPsiEs->i_type = TS_TYPE_MPEG1_VIDEO;
      else
        pDvbPsiEs->i_type = TS_TYPE_MPEG2_VIDEO;
    }
    // MPEG audio ES
    else if(iId >= PES_ID_MIN_AUDIO && iId <= PES_ID_MAX_AUDIO)
    {
      printf("Audio: 0x%x , %d\n", pDescr->GetPid(), pDescr->GetPid());
      if(m_iMpegVersion == 1)
        pDvbPsiEs->i_type = TS_TYPE_MPEG1_AUDIO;
      else
        pDvbPsiEs->i_type = TS_TYPE_MPEG2_AUDIO;
    }
    // MPEG Private 1 ES
    else if(iId == PES_ID_PRIVATE_1)
    {
      printf("Private 1: SubId: 0x%x , Pid: 0x%x , %d\n",
             iSubId, pDescr->GetPid(), pDescr->GetPid());
      // subtitles
      if((iSubId >= PES_PRIV_ID_MIN_SPU) &&
         (iSubId <= PES_PRIV_ID_MAX_SPU))
        pDvbPsiEs->i_type = TS_TYPE_SPU;
      // ac3 audio stream
      else if((iSubId >= PES_PRIV_ID_MIN_AC3) &&
              (iSubId <= PES_PRIV_ID_MAX_AC3))
        pDvbPsiEs->i_type = TS_TYPE_AC3;
      // lpcm audio stream
      else if((iSubId >= PES_PRIV_ID_MIN_LPCM) &&
              (iSubId <= PES_PRIV_ID_MAX_LPCM))
        pDvbPsiEs->i_type = TS_TYPE_LPCM;
      // other
      else
        pDvbPsiEs->i_type = TS_TYPE_MPEG2_PRIVATE;
    }
    // Padding
    else if(iId == PES_ID_PADDING)
    {
      pDvbPsiEs->i_type = TS_TYPE_MPEG2_PRIVATE;
    }
    else
    {
      pDvbPsiEs->i_type = TS_TYPE_MPEG2_PRIVATE;
    }

    printf("PMT Add, PID : 0x%x , Type : 0x%x\n",
           pDescr->GetPid(), pDvbPsiEs->i_type);
    m_bNewPmt = true;

    // Check if this pgrm must become the PCR_PID
    if(    (m_sPmt.i_pcr_pid == 0)
        && (iId >= PES_ID_MIN_VIDEO)
        && (iId <= PES_ID_MAX_VIDEO))
    {
      printf("updating PCR_PID to value %d (current pid = %d)\n",
             pDvbPsiEs->i_pid, m_sPmt.i_pcr_pid);

      m_sPmt.i_pcr_pid = pDvbPsiEs->i_pid;
    }
  }
  
  return pDescr;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_ElementDescriptor* C_ProgramDescriptor::GetDescriptor(u8 iId, u8 iSubId)
{
  C_ElementDescriptor* pDescr;
  if(iId != PES_ID_PRIVATE_1)
    pDescr = m_apElementaryStreams[iId];
  else
    pDescr = m_apP1ElementaryStreams[iSubId];
  if(!pDescr)
  {
    // Add the descriptor
    pDescr = AddES(iId, iSubId);
  }
  
  return pDescr;
}

